<?php

require_once '__CONSTANTS.php';

require_once LIBRARY_SE;
require_once CLASS_Referrer;
require_once CLASS_Extfile;
require_once CLASS_Statistic;
require_once CLASS_GEO;
require_once CLASS_Blacklist;

require_once LIBRARY_DEPRECATED_FUNCTIONS;

$_EXT = new EXTFILE( EXTSWA_EXTFILE_PATH );

if(empty($_SERVER['HTTP_CF_IPCOUNTRY']))
{
    $reg = new SxGeo(SXFILELIB);
    $_COUNTRY = $reg->getCountry($_SERVER['REMOTE_ADDR']);
}
else
    $_COUNTRY = $_SERVER['HTTP_CF_IPCOUNTRY'];

$settings = json_decode(get_option('extswa_settings'), true);
$_COUNTRY = $reg->getCountry($_SERVER['REMOTE_ADDR']);

$constants = array('ignor' => 'USE_EXMASKS', 'filename' => 'NEW_FILENAME', 'source' => 'SOURCE_REFERRERS', 'filepass' => 'FILEPASS', 'real' => 'STATUS_MODE_REAL', 'timer' => 'RANGE_STATISTICS', 'lvl' => 'DEPTH_VIEW_BEFORE' );

if(!empty($_REQUEST['filename'])) $_REQUEST['filename'] = str_replace('/','',$_REQUEST['filename']);

foreach($constants as $key => $name)
	define($name, empty($settings[$key]) ? '' : $settings[$key]);


$_TYPES_ENABLED_GENERATION = explode(',', $settings['typegen']);

$_blacklist = new blacklist;

if (in_array(DYNAMIC_GENERATION, $_TYPES_ENABLED_GENERATION))
{
            $_used = array('referrers' => array(), 'useragents' => array());
            $useragents = statistic::getUserAgents();
            $useragents = array_merge($useragents, json_decode(get_option('extswa_useragents','[]'),true));

            if(in_array(SOURCE_REFERRERS, array('all','stats')))
            {
                switch (RANGE_STATISTICS)
                {
                    case 'today':
                        $osk = 'AND `date`="'.EXTSWA_DATE.'"';
                    break;
                    case '1dl':
                        $osk = 'AND (`date`="'.EXTSWA_DATE.'" OR `date`="' . date('d.m.Y', strtotime('-1 day')) . '")';
                    break;
                    case '7dl':
                        $osk = 'AND (`date`="'.EXTSWA_DATE.'"';
                        for( $i = 1; $i < 7; $i++ )
                            $osk .= ' OR `date` = "' .date('d.m.Y', strtotime("-{$i} day")). '"';
                        $osk .= ')';
                    break;
                    case 'all':
                        $osk = '';
                    break;
                    default:
                        $osk = 'AND `date`="'.EXTSWA_DATE.'"';
                    break;
                }

                $a      = array();

                $answer = $wpdb->get_results('SELECT * FROM ' . EXTSWA_DB_STATS . " WHERE `type`=1 $osk AND `links`!=''", 'ARRAY_A');
                foreach( $answer as $row )
                {
                    $json = json_decode($row['links'], true);
                    for( $i = 0; $i < count($json); $i++ )
                    {
                        $a[$json[$i]][] = $row['source'];
                        $countarr[]     = $row['count'];
                    }
                }
                    
                if(STATUS_MODE_REAL == ENABLED || STATUS_MODE_REAL == SINGLE_PASS)
                    $reports = statistic::getReportsReferrers($osk);

                foreach($a as $key_link => $referrer)
                {
                    $_used['UserAgents'] = array();
                    $item_array = array('Pages' => array(), 'Referers' => array(), 'UserAgents' => array(), 'Paths' => array(), 'AfterPaths' => array());
                    $count_referrers = count($a[$key_link]);

                    $item_array['Pages'][] = array( 'Page' => $key_link, 'Priority' => rand(1, count($a) * 2));
                    for( $i = 0; $i < $count_referrers; $i++ )
                    {
                        $without_lr = dellr($a[$key_link][$i]);
                        if(!in_array($without_lr, $_used['referrers']))
                        {
                            if($_blacklist->inBlacklist($without_lr, 'source')) continue;
                            $_used['referrers'][] = setYandexLr($without_lr, $_COUNTRY);

                            $referrer = array('Referer' => $without_lr, 'Priority' => $countarr[$i]);

                            if(STATUS_MODE_REAL == ENABLED || STATUS_MODE_REAL == SINGLE_PASS)
                            {
                                $struct_referrer = reftostr($referrer['Referer']);
                                if(in_array($struct_referrer, $reports['ref']))
                                    $referrer['Priority'] -= $history['count'][array_search($struct_referrer, $reports['ref'])];
                            }

                            if($referrer['Priority'])
                                $item_array['Referers'][] = $referrer;
                        }
                    }

                    //SINGLE_PASS
                    if(empty($item_array['Referers']) && STATUS_MODE_REAL == SINGLE_PASS)
                        $item_array['Referers'][] = array('Priority' => 1, 'Referer' => $_used['referrers'][array_rand($_used['referrers'])]);

                    for($i = 0; $i < rand(4, 5); $i++)
                        $item_array['Paths'][]   = array('Path' => array_fill(0, rand_lvl(), '/'), 'Priority' => rand(1,300));

                    for($i = 0; $i < rand(4, 5); $i++)
                        $item_array['AfterPaths'][]   = array('Path' => array_fill(0, rand_lvl(true), '/'), 'Priority' => rand(1,300));

                    for ($i = 0; $i < rand(4, 5); $i++)
                    {
                        $useragent = $useragents[array_rand($useragents)];
                        if (!in_array($useragent, $_used['useragents']))
                        {
                            $_used['useragents'][] = $useragent;
                            if($_blacklist->inBlacklist($useragent, 'useragent')) continue;
                            $item_array['UserAgents'][] = array( 'UserAgent' => $useragent, 'Priority' => rand(1, count($useragent)));
                        }
                    }

                    if(!empty($item_array['Pages']) && !empty($item_array['Referers']) && !empty($item_array['UserAgents']))
                        $Item['Items'][] = $item_array;
                }

            }

            if(in_array(SOURCE_REFERRERS, array('pse','all')))
            {
                $psereferrers = json_decode(get_option('extswa_psereferrers','[]'), true);
                $count_psereferrers = count($psereferrers);

                for($i = 0; $i < $count_psereferrers; $i++)
                {
                    $_used['UserAgents'] = array();
                    if(!isAvailableSE($psereferrers[$i]['se']))
                        $psereferrers[$i]['se'] = getRandomSE();
                    $item_array = array('Pages' => array(), 'Referers' => array(), 'UserAgents' => array(), 'Paths' => array(), 'AfterPaths' => array());
                    $item_array['Pages'][] = array('Page' => $psereferrers[$i]['url'], 'Priority' => rand(1, $count_psereferrers * 2));

                    $generated_referer = referrer::$psereferrers[$i]['se']($psereferrers[$i]['referer'], false, rand(0,5), $_COUNTRY);
                    if($_blacklist->inBlacklist($generated_referer, 'source')) continue;
                    $item_array['Referers'][] = array('Referer' => $generated_referer, 'Priority' => rand(1, $count_psereferrers * 2));

                    for ($j = 0; $j < rand(4, 5); $j++)
                        $item_array['Paths'][]   = array('Path' => array_fill(0, rand_lvl(), '/'), 'Priority' => rand(1,300));

                    for($j = 0; $j < rand(4, 5); $j++)
                        $item_array['AfterPaths'][]   = array('Path' => array_fill(0, rand_lvl(true), '/'), 'Priority' => rand(1,300));
                        
                    for ($j = 0; $j < rand(4, 5); $j++)
                    {
                        $useragent = $useragents[array_rand($useragents)];
                        if (!in_array($useragent, $_used['useragents']))
                        {
                            $_used['useragents'][] = $useragent;
                            if($_blacklist->inBlacklist($useragent, 'useragent')) continue;
                            $item_array['UserAgents'][] = array( 'UserAgent' => $useragent, 'Priority' => rand(1, count($useragent)));
                        }
                    }

                    if(!empty($item_array['Pages']) && !empty($item_array['Referers']) && !empty($item_array['UserAgents']))
                        $Item['Items'][] = $item_array;
                }
            }

            $Item['Report'] = EXTSWA_REPORT_SCRIPT_URL;
            if (USE_EXMASKS == ENABLED)
            {
                if($exmasks = json_decode(get_option('extswa_exmasks','[]'), true))
                    $Item['ExMasks'] = $exmasks;
            }

            $checkpages = json_decode(get_option('extswa_checkpages', '[]'), true);
            if(!empty($checkpages))
                $Item['CheckPages'] = $checkpages;

            foreach(array('extswa_extensions' => 'Extensions', 'extswa_exextensions' => 'ExExtensions', 'extswa_mimetypes' => 'MimeTypes', 'extswa_exmimetypes' => 'ExMimeTypes') as $option => $key)
                $Item[$key] = json_decode(get_option($option,'[]'), true);

            $random_item = $Item['Items'][array_rand($Item['Items'])];
            $Item['Items'] = array($random_item);
	echo stripslashes(json_encode($Item));
}
?>