<?php

require_once LIBRARY_SE;

$settings = json_decode(get_option('extswa_settings'), true);

$enabled_se = getEnabledSE();

if(!empty($settings))
{
	$script ='$("input[name=rtime][value='.$settings['timer'].']").attr("checked","checked");';
	$script.='$("input[name=rignor][value='.$settings['ignor'].']").attr("checked","checked");';
	$script.='$("input[name=rsource][value='.$settings['source'].']").attr("checked","checked");';
	$script.='$("input[name=rlvl][value='.$settings['lvl'].']").attr("checked","checked");';
	$script.='$("input[name=rlvlafter][value='.$settings['lvlafter'].']").attr("checked","checked");';
	$script.='$("input[name=rreal][value='.$settings['real'].']").attr("checked","checked");';

	$gentype = array_diff(explode(',', $settings['typegen']), array(''));

	for( $i = 0; $i < count($gentype); $i++ )
		$script.='$("input[name=typegen][value='.$gentype[$i].']").attr("checked","checked");';

	$script.='$("select option[value='.$settings['autogen'].']").attr("selected","selected");';

	foreach($enabled_se as $se)
		$script.='$("input.se[se=\''.$se.'\']").attr("checked","checked");';

}
?>
<script>
jQuery(function($) {
	<?= empty($script) ? '' : $script ?>

	$('#bgen').click(function(){
		var arr = [],se = [], percents = {};
		$('input[name=typegen]:checked').each(function(){
			arr.push($(this).val());
		});

		$('input.se:checked').each(function(){
			se.push($(this).attr('se'))
		});

		$('.percent').each(function(){
			percents[$(this).attr('se')] = $(this).val();
		})

		$.post('<?=EXTSWA_AJAX_URL?>',{ action:'change_settings', percent : percents, _cajax:'<?=EXTSWA_AJAXPASS?>', 'enabled_se': ''+se,ignor:$('input[name=rignor]:checked').val(),real:$('input[name=rreal]:checked').val(),timer:$('input[name=rtime]:checked').val(),lvl:$('input[name=rlvl]:checked').val(),lvlafter:$('input[name=rlvlafter]:checked').val(),typegen:''+arr,autogen:$('#autogen').val(),filename:$('#filename').val(),filepass:$('#filepass').val(),source:$('input[name=rsource]:checked').val()}, function(data){
			$('div.nst').html(data);
		});
	});
});
</script>
<div class='nst' style="margin-top:5%;">
<h1>Настройка</h1>
Опции генерации:

<table>
	<tr>
		<td colspan='2'>*Диапазон статистики:</td>
		<td colspan='2'>Источник рефереров:</td>
		<td colspan='2'>Глубина просмотра:</td>
		<td colspan='2'>Глубина просмотра после клика:</td>
		<td colspan='2'>Игнор. Маски</td>
		<td colspan='2'>**Фильтр накрутки:</td>
	</tr>
	<tr>
		<td>Текущий день:</td>
		<td><input type='radio' class='rtime' checked name='rtime' value='today'></td>
		<td>Статистика:</td>
		<td><input type='radio' checked class='rsource' name='rsource' value='stats'></td>
		<td>2 - 5</td>
		<td><input type='radio' class='rlvl' checked name='rlvl' value='2t5'></td>
		<td>2 - 5</td>
		<td><input type='radio' class='rlvlafter' checked name='rlvlafter' value='2t5'></td>
		<td>Использовать:</td>
		<td><input type='radio' class='rignor' checked name='rignor' value='y'></td>
		<td>Выключен</td>
		<td><input type='radio' checked class='rreal' name='rreal' value='n'></td>
	</tr>
	<tr>
		<td>2 дня:</td>
		<td><input type='radio' class='rtime' name='rtime' value='1dl'></td>
		<td>Псевдо:</td>
		<td><input type='radio' class='rsource' name='rsource' value='pse'>
		<td>2 - 9</td>
		<td><input type='radio' class='rlvl' name='rlvl' value='2t9'></td>
		<td>2 - 9</td>
		<td><input type='radio' class='rlvlafter' name='rlvlafter' value='2t9'></td>
		<td>Не использовать:</td>
		<td><input type='radio' class='rignor' name='rignor' value='n'></td>
		<td>Включен</td>
		<td><input type='radio' class='rreal' name='rreal' value='y'></td>
	</tr>
	<tr>
		<td>Неделя:</td>
		<td><input type='radio' class='rtime' name='rtime' value='7dl'></td>
		<td>Смесь:</td>
		<td><input type='radio' class='rsource' name='rsource' value='all'></td>
		<td>2 - 13</td>
		<td><input type='radio' class='rlvl' name='rlvl' value='2t13'></td>
		<td>2 - 13</td>
		<td><input type='radio' class='rlvlafter' name='rlvlafter' value='2t13'></td>
		<td></td>
		<td></td>
		<td>Един. пропуск</td>
		<td><input type='radio' class='rreal' name='rreal' value='e'></td>
	</tr>
	<tr>
		<td>Все время:</td>
		<td><input type='radio' class='rtime' name='rtime' value='all'></td>
	</tr>
</table>
<br>*Диапазон статистики - время, за которое следует брать данные для составления файла Внешнего Источника.<br>**Фильтр накрутки - режим использования данных статистики, при котором ставиться приоритет рефереру на основе полученных отчетов. Единичный пропуск - режим активной фильтрации, но в случае если для Item'а не осталось ни одного настоящего реферера, добавляется один накрученный с минимальным приоритетом.<br><hr>
<br>Общие настройки:
<table>
	<tr>
		<td colspan='2'>*Использование настроек:</td>
		<td colspan='2'>Автоматическая генерация:</td>
		<td>Файл Внешнего Источника:</td>
		<td>Пароль:</td>
	</tr>
	<tr>
		<td>Динамическая генерация:</td>
		<td><input type='checkbox' name='typegen'  value='d'></td>
		<td>Состояние:</td>
		<td>
			<select id='autogen'>
				<option value='y'>Включено</option>
				<option value='n'>Выключено</option>
			</select>
		</td>
		<td><input id='filename' value='<?=EXTSWA_EXTFILE_NAME?>'></td>
		<td><input id='filepass' value=''></td>
	</tr>
	<tr>
		<td>Статическая генерация:</td>
		<td><input type='checkbox' name='typegen' value='s'></td>
	</tr>
</table>
<br>
*Поставьте галочку напротив метода(-ов) генерации для которых необходимо использовать уже заполненную форму. Если не поставите галочку напротив 'Динамическая генерация', она работать не будет.
<br/><hr>Включенные поисковые системы для фейковых реферреров:
<table>
	<tr><td>Google</td><td><input type="checkbox" class="se" se="google"></td><td>Yandex</td><td><input type="checkbox" class="se" se="yandex"></td></tr>
	<tr><td>Mail</td><td><input type="checkbox" class="se" se="mail"></td><td>Rambler</td><td><input type="checkbox" class="se" se="rambler"></td></tr>
	<tr><td>Yahoo</td><td><input type="checkbox" class="se" se="yahoo"></td><td>Bing</td><td><input type="checkbox" class="se" se="bing"></td></tr>
	<tr><td>Nigma</td><td><input type="checkbox" class="se" se="nigma"></td><td>QiP</td><td><input type="checkbox" class="se" se="qip"></td></tr>
	<tr><td>Ссылки</td><td><input type="checkbox" class="se" se="custom"></td></tr>
</table>
<br/><hr>Процентное распределение рандома:
<table>
	<?php foreach(getAvailableSE(array('custom')) as $name): ?>
		<tr><td><?=seToString($name)?></td><td><input type="text" class="percent" se="<?=$name?>" value="<?=getPercentSE($name)?>"></td></tr>
	<?php endforeach; ?>
</table>
<input id='bgen' type='button' value='Сохранить'></div>