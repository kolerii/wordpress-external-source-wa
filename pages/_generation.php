<?php
$script = '';
if($settings = json_decode(get_option('extswa_settings'), true))
{
	$script ='$("input[name=rtime][value='.$settings['timer'].']").attr("checked","checked");';
	$script.='$("input[name=rignor][value='.$settings['ignor'].']").attr("checked","checked");';
	$script.='$("input[name=rsource][value='.$settings['source'].']").attr("checked","checked");';
	$script.='$("input[name=rlvl][value='.$settings['lvl'].']").attr("checked","checked");';
	$script.='$("input[name=rlvlafter][value='.$settings['lvlafter'].']").attr("checked","checked");';
	$script.='$("input[name=rreal][value='.$settings['real'].']").attr("checked","checked");';
}
?>
<script>
jQuery(function($){
	<?=$script?>
	$('#bgen').click(function(){
		$.get('<?=EXTSWA_AJAX_URL?>', {action: 'generate_extsource_file',_cajax:'<?=EXTSWA_AJAXPASS?>', ignor:$('input[name=rignor]:checked').val(), real:$('input[name=rreal]:checked').val(),timer:$('input[name=rtime]:checked').val(),lvl:$('input[name=rlvl]:checked').val(),lvlafter:$('input[name=rlvlafter]:checked').val(),source:$('input[name=rsource]:checked').val()}, function(data){
			$('div.nst').html(data);
		});
	});
});
</script>
<div class='nst'>
<h1>Генерация Внешнего Источника</h1>
<?php if(!empty($settings['typegen']) && in_array('s',explode(',',$settings['typegen']))):?>
	Дополнительные опции:
	<table>
		<tr>
			<td colspan='2'>*Диапазон статистики:</td>
			<td colspan='2'>Источник рефереров:</td>
			<td colspan='2'>Глубина просмотра:</td>
			<td colspan='2'>Глубина просмотра после клика:</td>
			<td colspan='2'>Игнор. Маски</td>
			<td colspan='2'>**Фильтр накрутки:</td>
		</tr>
		<tr>
			<td>Текущий день:</td>
			<td><input type='radio' class='rtime' checked name='rtime' value='today'></td>
			<td>Статистика:</td>
			<td><input type='radio' checked class='rsource' name='rsource' value='stats'></td>
			<td>2 - 5</td>
			<td><input type='radio' class='rlvl' checked name='rlvl' value='2t5'></td>
			<td>2 - 5</td>
			<td><input type='radio' class='rlvl' checked name='rlvlafter' value='2t5'></td>
			<td>Использовать:</td>
			<td><input type='radio' class='rignor' checked name='rignor' value='y'></td>
			<td>Выключен</td>
			<td><input type='radio' checked class='rreal' name='rreal' value='n'></td>
		</tr>
		<tr>
			<td>2 дня:</td>
			<td><input type='radio' class='rtime' name='rtime' value='1dl'></td>
			<td>Псевдо:</td>
			<td><input type='radio' class='rsource' name='rsource' value='pse'>
			<td>2 - 9</td>
			<td><input type='radio' class='rlvl' name='rlvl' value='2t9'></td>
			<td>2 - 9</td>
			<td><input type='radio' class='rlvl' name='rlvlafter' value='2t9'></td>
			<td>Не использовать:</td>
			<td><input type='radio' class='rignor' name='rignor' value='n'></td>
			<td>Включен</td>
			<td><input type='radio' class='rreal' name='rreal' value='y'></td>
		</tr>
		<tr>
			<td>Неделя:</td>
			<td><input type='radio' class='rtime' name='rtime' value='7dl'></td>
			<td>Смесь:</td>
			<td><input type='radio' class='rsource' name='rsource' value='all'></td>
			<td>2 - 13</td>
			<td><input type='radio' class='rlvl' name='rlvl' value='2t13'></td>
			<td>2 - 13</td>
			<td><input type='radio' class='rlvl' name='rlvlafter' value='2t13'></td>
			<td></td>
			<td></td>
			<td>Един. пропуск</td>
			<td><input type='radio' class='rreal' name='rreal' value='e'></td>
		</tr>
		<tr>
			<td>Все время:</td>
			<td><input type='radio' class='rtime' name='rtime' value='all'></td>
		</tr>
	</table>
	<br>*Диапазон статистики - время, за которое следует брать данные для составления файла Внешнего Источника.
	<br>**Фильтр накрутки - режим использования данных статистики, при котором ставиться приоритет рефереру на основе полученных отчетов. Единичный пропуск - режим активной фильтрации, но в случае если для Item'а не осталось ни одного настоящего реферера, добавляется один накрученный с минимальным приоритетом.
	<br><hr>
	Старый файл Внешнего Источника будет удален. Вы уверены в необходимости генерации Внешнего источника?
	<br><input id='bgen' type='button' value='Генерация'>
<?php else: ?>
	Статический метод генерации отключен.
<?php endif; ?>