<h1>Собранная статистика</h1>
<script>
jQuery(function($){
	$('.tab-h4').click(function(){
		var titles = ['Статистика по браузерам','Статистика по поисковым переходам','Статистика по накрученным посетителям','Статистика по реальным посетителям'],
			idx = $(this).attr('target');

		$('.tab-div').hide();
		$('.tab-h4').each(function(){
			$(this).removeClass('active');
			$(this).html('+' + titles[parseInt($(this).attr('target'))]);
		});

		if(!$(this).hasClass('active'))
		{
			$(this).addClass('active');
			$('[div-target="'+idx+'"]').show('slow');
			$(this).html('-' + titles[parseInt(idx)]+':');
		}
	});
});
</script>
<div id='nst'>
	<h4><div class='tab-h4' target="0">+Статистика по браузерам</div></h4>
	<div class="tab-div" style='display:none;' div-target="0"><?=@file_get_contents(EXTSWA_AJAX_URL.'?action=show_table_statistic&q=BROWSER&_cajax='.EXTSWA_AJAXPASS)?></div>

	<h4><div class='tab-h4' target="1">+Статистика по поисковым переходам</div></h4>
	<div class="tab-div" style='display:none;' div-target="1"><?=@file_get_contents(EXTSWA_AJAX_URL.'?action=show_table_statistic&q=SEARCH&_cajax='.EXTSWA_AJAXPASS)?></div>

	<h4><div class='tab-h4' target="2">+Статистика по накрученным посетителям</div></h4>
	<div class="tab-div" style='display:none;' div-target="2"><?=@file_get_contents(EXTSWA_AJAX_URL.'?action=show_table_statistic&q=F&_cajax='.EXTSWA_AJAXPASS)?></div>

	<h4><div class='tab-h4' target="3">+Статистика по реальным посетителям</div></h4>
	<div class="tab-div" style='display:none;' div-target="3"><?=@file_get_contents(EXTSWA_AJAX_URL.'?action=show_table_statistic&q=R&_cajax='.EXTSWA_AJAXPASS)?></div>
</div>