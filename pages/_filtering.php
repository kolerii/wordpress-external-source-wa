<h2>Фильтрация</h2>
<style>
    textarea.filters
    {
        resize: none;
        width:100%;
    }
</style>
<script>
    jQuery(function($){
        $('a.submit-filters').click(function(){
            $.post('<?=EXTSWA_AJAX_URL?>', {action:'save_lists_filters',_cajax:'<?=EXTSWA_AJAXPASS?>',ext:$('.ext').val(), exext:$('.exext').val(), mime:$('.mime').val(), exmime:$('.exmime').val(), urlsfilters:$('.urlsfilters').val()}, function(html){
                $('div.display').html(html);
            })
        })
    })
</script>
<div class="display">
    <table class="wp-list-table widefat">
        <thead>
        <tr>
            <th>Extensions</th>
            <th>ExExtensions</th>
            <th>MimeTypes</th>
            <th>ExMimeTypes</th>
            <th>URLFilters</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><textarea class="filters ext" rows="20"><?=implode("\n",json_decode(get_option('extswa_extensions','[]'),true))?></textarea></td>
            <td><textarea class="filters exext" rows="20"><?=implode("\n",json_decode(get_option('extswa_exextensions','[]'),true))?></textarea></td>
            <td><textarea class="filters mime" rows="20"><?=implode("\n",json_decode(get_option('extswa_mimetypes','[]'),true))?></textarea></td>
            <td><textarea class="filters exmime" rows="20"><?=implode("\n",json_decode(get_option('extswa_exmimetypes','[]'),true))?></textarea></td>
            <td><textarea class="filters urlsfilters" rows="20"><?=implode("\n",json_decode(get_option('extswa_urlsfilters','[]'),true))?></textarea></td>
        </tr>
        </tbody>
    </table>
    <br/>
    <a class="button button-primary button-large submit-filters">Сохранить</a>
</div>