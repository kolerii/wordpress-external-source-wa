<?php
	require_once LIBRARY_SE;
	$available_se = array();
	foreach(getNamesSE() as $key => $name)
	{
		if(isAvailableSE($key))
			$available_se[] = array('uid' => $key, 'name' => $name);
	}

?>
<script>
jQuery(function($){
	$("#addtolist").click(function(){
		addForm();
	});

	$("#savelist").click(function(){
		var referrers = [];
		$('.referrer-input-page').each(function(){
			var parent = $(this).parent().parent();
			referrers.push({'url':$(this).val(),'referer':parent.find('.referrer-input-query').val(), 'se':parent.find('.referrer-input-se').val()});
		});

		$.post("<?=EXTSWA_AJAX_URL?>", {action:'save_list_psereferrer',_cajax:'<?=EXTSWA_AJAXPASS?>',data:JSON.stringify(referrers)}, function(html){
			$("#nst").html(html);
		});
	});

	$('body').on('click', '.psereferrer-radio', function(){
		$(this).parent().parent().remove();
		refreshCount();
	});

	$('#clearlist').click(function(){
		$(".referrer-tbody").html('');
		refreshCount();
	});

	function addForm( page, query, se )
	{
		var enabled_se = <?php echo json_encode($available_se);?>,
			tr = '<tr><td><input type="button" value="х" class="psereferrer-radio"><td><input value="'+(page ? page : '')+'" class="referrer-input-page" style="width:100%;"></td><td><input value="'+(query ? query : '')+'" class="referrer-input-query" style="width:100%;"></td><td><select class="referrer-input-se">';

		enabled_se.forEach(function(e){
			tr += '<option'+(e['uid'] == se ? ' selected' : '')+' value="'+e['uid']+'">'+e['name']+'</option>';
		})
		$('.referrer-tbody').append(tr+'</select></td></tr>');
	}

	function refreshCount()
	{
		$('.count-referrers').html($('.referrer-tbody > tr').length);
	}

	function appendJSON( json )
	{
		var data = JSON.parse(json);
		data.forEach(function(e){
			addForm(e['page'], e['query'], e['se']);
		});
		refreshCount();
	}

	$('.gen-list').click(function(){
		var actions = {titles: 'generate_psereferrers_titles', contents: 'generate_psereferrers_contents', topmail: 'generate_psereferrers_topmail', vk: 'generate_psereferrers_vk'},
			type = $(this).attr('type-gen');
		if(type in actions)
		{
			var data = {action: actions[type], _cajax: '<?=EXTSWA_AJAXPASS;?>'};
			switch(type)
			{
				case 'contents':
					data['min'] = $('#contents-min').val();
					data['max'] = $('#contents-max').val();
				break;
				case 'topmail':
					data['id'] = $('.topmail-id').val();
				break;
				case 'vk':
					data['query'] = $('.vk-keyword').val();
					data['max'] = $('.vk-maxcount').val();
				break;
			}

			$.post("<?php echo EXTSWA_AJAX_URL;?>", data, function(json){
				appendJSON(json);
			})
		}
	});

	//Init
	$.get("<?php echo EXTSWA_AJAX_URL.'?action=get_list_psereferrers&_cajax='.EXTSWA_AJAXPASS; ?>", function(json){
		appendJSON(json);
	})
});
</script>
<style type="text/css">
	.tr-titles
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 0.85em;
		font-weight: bold;
		text-align: center;
	}
	.tr-button-input > td > input
	{
		width: 100%;
	}
	.count-referrers
	{
		font-weight: bold;
	}
</style>
<div id="nst">
	<h1>Составление списка фейковых рефереров</h1>
	<p>Действия: <input type="button" value="Добавление ячейки" id="addtolist" class="button button-primary button-large"> <input type="button" value="Очистить список" id="clearlist" class="button button-primary button-large"> <input type="button" value="Сохранить список" id="savelist" class="button button-primary button-large"></p>
	<p>
		Генераторы списка:
		<table>
			<tr class="tr-titles">
				<td>По заголовкам</td>
				<td>По фрагментам</td>
				<td>По группам ВК</td>
				<td>По фразам Top.Mail</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<table>
						<tr><td>Min:</td><td><input value="0" id="contents-min" style="width:80px;"></td></tr>
						<tr><td>Max:</td><td><input value="0" id="contents-max" style="width:80px;"></td></tr>
					</table>
				</td>
				<td>
					<table>
						<tr><td>Ключевое слово:</td><td><input type="text" placeholder="бизнес" class="vk-keyword" style="width:80px;"></td></tr>
						<tr><td>Max количество:</td><td><input type="text" placeholder="10" class="vk-maxcount" style="width:80px;"></td></tr>
					</table>
				</td>
				<td>
					ID: <input type="text" placeholder="0000" class="topmail-id" style="width:80px;"><br/>
				</td>
			</tr>
			<tr class="tr-button-input">
				<td><input type="button" value="Генерация списка" type-gen="titles" class="gen-list button button-primary button-large"></td>
				<td><input type="button" value="Генерация списка" type-gen="contents" class="gen-list button button-primary button-large"></td>
				<td><input type="button" value="Генерация списка" type-gen="vk" class="gen-list button button-primary button-large"></td>
				<td><input type="button" value="Генерация списка" type-gen="topmail" class="gen-list button button-primary button-large"></td>
			</tr>
		</table>
	</p><hr>
	Список фейковых рефереров ( всего <span class="count-referrers">0</span> ):
	<div id="tablelist">
	    <table class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <th style="width:30px;"></th><th>Страница</th><th>Запрос</th><th style="width:100px;">Поисковик</th>
                </tr>
            </thead>
            <tbody class="referrer-tbody"></tbody>
        </table>
    </div>
</div>