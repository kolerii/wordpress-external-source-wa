<h1>Структура сайта</h1>
<?php

$prelink = 'http://'.$_SERVER['HTTP_HOST'];
$urls = array('calendar' => array(), 'categories' => array(), 'posts' => array(), 'tags' => array());
$translate = array('posts' => 'Статьи', 'calendar' => 'Календарь', 'tags' => 'Теги', 'categories' => 'Категории');

$rows = $wpdb->get_results("SELECT id, post_date FROM {$wpdb->prefix}posts WHERE post_status='publish' OR post_status='inherit'", 'ARRAY_A');
foreach($rows as $row)
{
	$urls['posts'][] = str_replace( $prelink, '', get_permalink( $row['id'] ) );

	$calendar_url = '/?m=' . str_replace('-', '', @reset(explode(' ', $row['post_date'])));
	if(!in_array($calendar_url, $urls['calendar']))
		$urls['calendar'][] = $calendar_url;
}

$rows = $wpdb->get_results("SELECT term_id FROM {$wpdb->prefix}term_taxonomy WHERE taxonomy='category'", 'ARRAY_A');
foreach($rows as $row)
	$urls['categories'][] = '/?cat=' . $row['term_id'];

$rows = $wpdb->get_results("SELECT t2.slug FROM {$wpdb->prefix}term_taxonomy AS t1, {$wpdb->prefix}terms AS t2 WHERE t1.taxonomy='post_tag' AND t1.term_id=t2.term_id", 'ARRAY_N');
foreach($rows as $row)
	$urls['tags'][] = '/?tag=' . $row[0];

?>
<table class="wp-list-table widefat">
	<thead>
		<tr>
			<th>Календарь</th>
			<th>Категории</th>
			<th>Статьи</th>
			<th>Тэги</th>
		</tr>
	</thead>
	<tbody>
		<tr><?php
		foreach($urls as $key => $array_urls):?>
			<td><?php foreach($array_urls as $url): echo '<nobr>', $url,'</nobr>'; ?><br/><?php endforeach;?></td>
		<?php endforeach;?>
		</tr>
	</tbody>
</table>