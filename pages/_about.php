<h1>О плагине</h1>
<table style="margin-top:5%;">
	<tr>
		<td>Описание:</td>
		<td>Плагин для сбора поисковых рефереров и формирования внешнего источника.</td>
	</tr>
	<tr>
		<td>Разработчик:</td>
		<td>Беляков Владислав [kolerii]</td>
	</tr>
	<tr>
		<td>Новости плагина:</td>
		<td><a href="http://kolerii.ru/?cat=93" target="_blank">Блог</a> | <a href="https://bitbucket.org/kolerii/wordpress-external-source-wa/downloads" target="_blank">Последняя версия</a></td>
	</tr>
	<tr>
		<td>Версия плагина:</td>
		<td><?=EXTSWA_VERSION?></td>
	</tr>
	<tr>
		<td><b>Файлы:</b></td>
	</tr>
	<tr>
		<td>Файл Внешнего Источника:</td>
		<td><?=EXTSWA_EXTFILE_URL?></td>
	</tr>
	<tr>
		<td>Скрипт отчетов:</td>
		<td><?=EXTSWA_PLUGIN_URL?>wp-reports.php</td>
	</tr>
	<tr>
		<td>Команда для крона:</td>
		<td>wget <?=EXTSWA_PLUGIN_URL?>cron.php</td>
	</tr>
	<tr>
		<td>Файл генерации одиночного Item'a:</td>
		<td><?=EXTSWA_PLUGIN_URL?>single.php</td>
	</tr>
	<tr>
		<td><b>Для благодарностей:</b></td>
	</tr>
	<tr>
		<td>WMR Webmoney: </td><td>R818293885861</td>
	</tr>
	<tr>
		<td>WMZ Webmoney: </td><td>Z387661783541</td>
	</tr>
	<tr>
		<td>WMB Webmoney: </td><td>B131803528108</td>
	</tr>
	<tr>
		<td><b>Дебаг и пожелания:</b></td>
	</tr>
	<tr>
		<td colspan="2">
			Всех, кому необходим или интересен этот плагин, прошу отписываться насчет багов или пожеланий непосредственно в комментариях под постом версии в блоге, к которой содержание сообщения имеет отношение. Это необходимо для предотвращения массовых повторяющихся отчетов об ошибках или идей. Я буду писать по посту об обновлении для каждой 2.х версии 
		</td>
	</tr>
</table>