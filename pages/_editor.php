<?php require_once CLASS_Extfile; require_once LIBRARY_DEPRECATED_FUNCTIONS; ?>
<h1>Редактирование Файла Внешнего Источника</h1>
<style>
	#extsourcetxt
	{
		height: 70%;
		width: 98%;
	}
</style>
<?php
	if(!empty($_POST['source-file']))
	{
		$_EXT = new EXTFILE(EXTSWA_EXTFILE_PATH);
		$_EXT->setData($_POST['source-file'], true) ? alert('Файл успешно сохранен!') : alert('При сохранении произошла какая-то ошибка!', true);
	}
?>
<div>
	<form method="POST">
		<textarea name="source-file" id="extsourcetxt" rows="25"><?=file_exists(EXTSWA_EXTFILE_PATH) ? file_get_contents(EXTSWA_EXTFILE_PATH) : ''?></textarea>
		<br><input type="submit" class="button button-primary button-large" value="Сохранить">
	</form>
</div>