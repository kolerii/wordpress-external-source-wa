<h2>Дополнительные UserAgent'ы</h2>
<style>
    textarea.filters
    {
        resize: none;
        width:100%;
    }
</style>
<script>
    jQuery(function($){
        $('a.submit-filters').click(function(){
            $.post('<?=EXTSWA_AJAX_URL?>', {action:'save_lists_useragents',_cajax:'<?=EXTSWA_AJAXPASS?>',useragents:$('.useragents').val()}, function(html){
                $('div.display').html(html);
            })
        })
    })
</script>
<div class="display">
    <textarea class="filters useragents" rows="20"><?=implode("\n",json_decode(get_option('extswa_useragents','[]'),true))?></textarea></td>
    <br/>
    <a class="button button-primary button-large submit-filters">Сохранить</a>
</div>