<?php require_once CLASS_Extfile; require_once LIBRARY_DEPRECATED_FUNCTIONS; ?>
<h1>Валидация Внешнего Источника</h1>
<script type="text/javascript">
jQuery(function($){
	$('.copy-default-url').click(function(){
		$('[name="url-external"]').val($(this).html());
	})
})
</script>
<div>
	<form method="POST">
		URL: <input name="url-external" placeholder="http://example.com/ext.txt" style="width:70%">
		<input type="submit" class="button button-primary button-large" value="Проверка"><br>
		Внешний Источник данного сайта: <b class="copy-default-url"><?=EXTSWA_EXTFILE_URL?></b>
		<br/><i>(Кликните по ссылке для копирования в поле адреса.)</i>
	</form>
</div>
<?php if(!empty($_REQUEST['url-external'])):?>
	<style type="text/css">
		table.stats th
		{
			float:left;
		}
		table.stats td
		{
			float:right;
		}
	</style>
	<hr/>
	<div>
		<?php
			$_extfile = new EXTFILE($_REQUEST['url-external']);
			if(!$_extfile->_empty())
			{
				$countCorrentItems = $_extfile->count(true);
				$countItems = $_extfile->count();
				if($countCorrentItems == 0) alert('Файл Внешнего Источника полностью некорректен!');
				elseif($countCorrentItems != 0 && $countCorrentItems < $countItems) alert('Файл Внешнего Источника корректен не полностью!', true);
				if(empty($_extfile->report)) alert('Для работы плагина должен был указан файл отчетов!', true);
				?><table class="table stats">
					<tr>
						<th>Количество корректных Item'ов:</th><td><?php echo $countCorrentItems?>/<?php echo $countItems?></td>
					</tr>
					<tr>
						<th>Количество UserAgents:</th><td><?php echo $_extfile->calc('ua');?></td>
					</tr>
					<tr>
						<th>Количество Referrer:</th><td><?php echo $_extfile->calc('ref');?></td>
					</tr>
					<tr>
						<th>Количество Page:</th><td><?php echo $_extfile->calc('pages');?></td>
					</tr>
					<tr>
						<th>Количество путей до клика:</th><td><?php echo $_extfile->calc('paths');?></td>
					</tr>
					<tr>
						<th>Количество путей после клика:</th><td><?php echo $_extfile->calc('after_paths');?></td>
					</tr>
					<tr>
						<th>Файл отчетов:</th><td><?php echo empty($_extfile->report) ? 'Не указан' : 'Указан'?></td>
					</tr>
				</table><?php
			}
			else
				alert('Файл Внешнего Источника пуст или не существует.', true);
		?>
	</div>
<?php endif;?>