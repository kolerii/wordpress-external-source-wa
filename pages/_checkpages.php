<h2>Настройка проверки доступности</h2>
<script type="text/javascript">
	jQuery(function($){
		function checkBlockNothing()
		{
			$('.table-checkpages, .nothing-block').hide();

			if($('.tbody-checkpages > tr').length > 0)
				$('.table-checkpages').show();
			else
				$('.nothing-block').show();
		}

		$('body').on('click','.checkpage-remove', function(){
			$(this).parent().parent().remove();
		});

		$('.add-form-checkpage').click(function(){
			$('.tbody-checkpages').append('<tr class="checkpage-tr"><td><input type="button" value="х" class="checkpage-remove"></td><td><input type="text" class="page"></td><td><input type="text" class="ip"></td><td><input type="text" class="substr"></td><td><input type="text" class="useragent"></td></tr>');
			checkBlockNothing();
		});

		$('.submit-checkpages').click(function(){
			var arr = [];
			$('.checkpage-tr').each(function(){
				arr.push({
							'page'      : $(this).find('.page').val(),
							'ip'        : $(this).find('.ip').val(),
							'substr'    : $(this).find('.substr').val(),
							'useragent' : $(this).find('.useragent').val()
				});
			});

			$.post("<?=EXTSWA_AJAX_URL?>",{action:'save_list_checkpages',_cajax:'<?=EXTSWA_AJAXPASS?>', data: JSON.stringify(arr)}, function(html){
				$('.display').html(html);
			})
		})

	})
</script>
<table>
	<tr>
		<td><a class="button button-primary button-large submit-checkpages">Сохранить</a></td>
		<td><a class="button button-primary button-large add-form-checkpage">Добавить форму</a></td>
	</tr>
</table>
<div class="display"></div>
<?php $data = json_decode(get_option('extswa_checkpages', '[]'), true);?>
<table class="wp-list-table widefat table-checkpages" <?=empty($data) ? 'style="display:none;"' : ''?>>
    <thead>
    <tr>
    	<th>#</th>
        <th>Страница</th>
        <th>IP хоста</th>
        <th>Подстрока</th>
        <th>UserAgent</th>
    </tr>
    </thead>
    <tbody class="tbody-checkpages">
    	<?php foreach($data as $element):?>
    		<tr class="checkpage-tr">
    			<td><input type="button" value="х" class="checkpage-remove"></td>
    			<td><input type="text" class="page" value="<?=$element['Page']?>"></td>
    			<td><input type="text" class="ip" value="<?=$element['ExpectedIP']?>"></td>
    			<td><input type="text" class="substr" value="<?=$element['IncludedText']?>"></td>
    			<td><input type="text" class="useragent" value="<?=$element['UserAgent']?>"></td>
    		</tr>
    	<?php endforeach;?>
    </tbody>
</table>
<center class="nothing-block" <?=!empty($data) ? 'style="display:none;"' : ''?>><h3>Вы не добавили ни одного элемента!</h3></center>