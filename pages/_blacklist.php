<h2>Черные списки</h2>
<?php $data = json_decode(get_option('extswa_blacklist','[]'), true); ?>
<script type="text/javascript">
	jQuery(function($){
		$('.blacklist-add').click(function(){
			$('.blacklist-tbody').append('<tr class="tr-blacklist-item"><td><input type="button" value="х" class="blacklist-remove"></td><td><input type="text" class="substr-blacklist"></td><td><select class="type-blacklist"><option selected value="useragent">UserAgent</option><option value="source">Источник</option></select></td></tr>');
		});
		$('body').on('click','.blacklist-remove', function(){
			$(this).parent().parent().remove();
		});
		$('.blacklist-save').click(function(){
			var arr = [];
			$('.tr-blacklist-item').each(function(){
				arr.push({
					'substr' : $(this).find('.substr-blacklist').val(),
					'type'   : $(this).find('.type-blacklist').val()
				})
			});

			$.post('<?=EXTSWA_AJAX_URL?>', {action:'save_blacklist',_cajax:'<?=EXTSWA_AJAXPASS?>', data: JSON.stringify(arr)}, function(html){
				$('.display').html(html);
			});
		});
	})
</script>
<div class="display"></div>
<div id="nst">
	<a class="button button-primary button-large blacklist-add">Добавить форму</a> <a class="button button-primary button-large blacklist-save">Сохранить</a><br><br>
	<div id="tablelist">
		<table class="wp-list-table widefat fixed posts">
			<thead>
				<tr>
					<th style="width:50px;"></th><th>Подстрока</th><th>Тип данных</th>
				</tr>
			</thead>
			<tbody class="blacklist-tbody">
				<?php foreach($data as $item):?>
					<tr class="tr-blacklist-item">
						<td><input type="button" value="х" class="blacklist-remove"></td>
						<td><input type="text" class="substr-blacklist" value="<?=$item['substr']?>" style="width:100%;"></td>
						<td>
							<select class="type-blacklist">
								<option <?=$item['type'] == 'useragent' ? 'selected' : ''?> value="useragent">UserAgent</option>
								<option <?=$item['type'] == 'source' ? 'selected' : ''?> value="source">Источник</option>
							</select>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>