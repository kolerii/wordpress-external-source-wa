<script>
jQuery(function($){

	function loadTable()
	{
		$.get('<?=EXTSWA_AJAX_URL?>', {action:'get_table_exmasks', _cajax:'<?=EXTSWA_AJAXPASS?>'}, function(data){
			$("#tablemasks").html(data);
		});
	}

	$('.exmasks-control').click(function(){
		switch($(this).attr('target'))
		{
			case 'refresh_list':
				loadTable();
			break;
			case 'save_exmasks':
				var inputs = [];
				$('.exmasks-input').each(function(){
					inputs.push($(this).val());
				});
				$.post('<?=EXTSWA_AJAX_URL?>', {action:'save_exmasks',_cajax:'<?=EXTSWA_AJAXPASS?>', data:JSON.stringify(inputs)}, function(data){
					$("#tablemasks").html(data);
				});
			break;
			case 'add_input':
				$('.exmasks-tbody').append('<tr><td><input type="button" value="х" class="exmasks-radio"></td><td><input type="text" class="exmasks-input" value=""></td></tr>');
			break;
		}
	});
	$('body').on('click', '.exmasks-radio', function(){
		$(this).parent().parent().remove();
	})

	loadTable();
});
</script>
<div id="nst">
	<h1>Составление масок игнорирования</h1><Br>
	<table>
		<tr>
			<td>Список:</td>
			<td>Действие:</td>
		</tr>
		<tr>
			<td><input type="button" value="Сохранить" class="exmasks-control" target="save_exmasks"></td>
			<td><input type="button" value="Добавить ячейку" class="exmasks-control" target="add_input"></td>
		</tr>
		<tr>
			<td><input type="button" value="Загрузить" class="exmasks-control" target="refresh_list"></td>
		</tr>
	</table>
	<div id="tablemasks"></div>
</div>