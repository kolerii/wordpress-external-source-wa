<?php

class browser
{
	private $referer;
	private $useragent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0';

	private $connect;

	private $page;
	private $last_open_url;

	public function __construct( $with_cookies, $with_redirects, $max_timeout = 30 )
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);

		if($with_cookies)
		{
			curl_setopt($ch, CURLOPT_COOKIEFILE,  'cookiefile');
			curl_setopt($ch, CURLOPT_COOKIEJAR,  'cookiefile');
		}

		if ($with_redirects)
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		curl_setopt($ch, CURLOPT_TIMEOUT, $max_timeout);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $max_timeout-1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);

		$this->connect = curl_copy_handle($ch);
	}
	///////////
	//Приватные функции вызываемые из getPage
	private function clear($html)
	{
		return preg_replace("'(?:[\r\n][\s]+|[\s]{1,})'","", $html);
	}
	//////
	private function cutScript($html)
	{
		return preg_replace("'<script[^>]*?>.*?</script>'si", "", $html);
	}
	//////
	private function cutStyle($html)
	{
		return preg_replace("'<style[^>]*?>.*?</style>'si", "", $html);
	}
	//
	///////////
	public function getPage( $targets = false )
	{
		$page = $this->page;

		if( !$targets )
			return $page;
		else
		{
			foreach( $targets as $v ) $page = $this->$v($page);

			return $page;
		}
	}
	public function go( $url, $post = false, $inc_ref = false )
	{
		$ch_copy = curl_copy_handle($this->connect);

		if( $post !== false )
		{
			curl_setopt($ch_copy, CURLOPT_POST, 1);
			curl_setopt($ch_copy, CURLOPT_POSTFIELDS, $post);
		}

		if(!empty($this->referer))
			curl_setopt($ch_copy, CURLOPT_REFERER, $this->referer);

		if($inc_ref !== false)
			$this->referer = $url;

		curl_setopt($ch_copy, CURLOPT_URL, $url);

		$html = trim(curl_exec($ch_copy));
		curl_close($ch_copy);
		$this->last_open_url = $url;

		$this->page = $html;	
		return $html;
	}

	public function setUA( $useragent )
	{
		$this->useragent = $useragent;
		curl_setopt($this->connect, CURLOPT_USERAGENT, $useragent);
	}

	public function setProxy( $proxy )
	{
		curl_setopt($this->connect, CURLOPT_PROXY, $proxy);
	}

	public function setRef( $referer )
	{
		$this->referer = $referer;
	}

	public function __destruct()
	{
		@unlink('cookiefile');
		curl_close($this->connect);
	}

}
?>
