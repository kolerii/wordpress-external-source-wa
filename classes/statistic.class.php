<?php
	class statistic
	{
        static public function getArrayPages()
        {
            global $wpdb;
            
            $return = array();
            $answer = $wpdb->get_results('SELECT ID,post_title FROM ' . $wpdb->prefix . 'posts WHERE post_status="publish"','ARRAY_A');
            foreach($answer as $row)
                $return[] = str_replace(EXTSWA_SITE_ROOT_URL, '', get_permalink($row['ID']));
            return $return;
        }

		static public function getReportsReferrers( $date = false )
		{
			global $wpdb;

			$date_query = $date ? $date : '`date`=' . EXTSWA_DATE . '"';

            $answer         = $wpdb->get_results("SELECT `count`,`source` FROM " . EXTSWA_DB_STATS . " WHERE {$date_query} AND `type`=2 ORDER BY `count` DESC", 'ARRAY_A');
            $reports = array('ref' => array(), 'count' => array());
            foreach($answer as $row)
            {
                $ta = json_decode($rep['source']. true);
                        
                $rstring = reftostr($ta['referrer']);
                if (!in_array($rstring, $reports['ref']))
                {
                    $reports['ref'][]     = $rstring;
                    $reports['count'][] = $rep['count'];
                }
                else
                    $reports['count'][array_search($rstring, $reports['ref'])] += $rep['count'];
            }
            return $reports;
		}

        static public function getUserAgents()
        {
            global $wpdb;
            return $wpdb->get_col("SELECT `source` FROM " . EXTSWA_DB_STATS . " WHERE  `type`=0");
        }

		static public function getRealReferrers()
		{
			$statistic = self::getReferrers();
			$reports = self::getReportsReferrers();

			$count_stat = count($statistic['ref']);

            for ($z = 0; $z < $count_stat; $z++)
            {
                if (in_array($statistic['ref'][$z], $reports['ref']))
                    $statistic['count'][$z] -= $reports['count'][array_search($statistic['ref'][$z], $reports['ref'])];
            }

            return $statistic;
		}

		static public function getReferrers($date = false)
		{
			global $wpdb;
			$date_query = $date ? $date : '`date`=' . EXTSWA_DATE . '"';
            $answer           = $wpdb->get_results("SELECT `count`,`source` FROM " . EXTSWA_DB_STATS . " WHERE {$date_query} AND `type`=1 ORDER BY `count` DESC", 'ARRAY_A');
            $statistic = array('ref' => array(), 'count' => array());
            foreach($answer as $row)
            {
                        
                $rstring = reftostr($slist['source']);
                if (!in_array($rstring, $statistic['ref']))
                {
                    $statistic['ref'][]     = $rstring;
                    $statistic['count'][] = $slist['count'];
                }
                else
                    $statistic['count'][array_search($rstring, $statistic['ref'])] += $slist['count'];
            }

            return $statistic;
		}
	}
?>