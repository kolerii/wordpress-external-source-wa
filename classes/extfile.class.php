<?php
	class EXTITEM
	{
		function __construct( &$array )
		{
			$this->_data = $array;

			$this->ua = &$this->_data['UserAgents'];
			$this->ref = &$this->_data['Referers'];
			$this->pages = &$this->_data['Pages'];
			$this->paths = &$this->_data['Paths'];
			$this->after_paths = &$this->_data['AfterPaths'];
		}

		public function validate()
		{
			$result = true;
			foreach( array('Pages','Paths','Referers','UserAgents', 'AfterPaths') as $key)
			{
				if(empty($this->_data[$key]) || empty($this->_data[$key][array_rand($this->_data[$key])]))
					$result = false;
			}
			return $result;
		}
	}

	class EXTFILE
	{
		function __construct( $filename )
		{
			$this->_filename = $filename;
			if(file_exists($filename) || strpos($filename, 'http') !== false)
			{
				$this->_data = json_decode(file_get_contents($filename), true);
				$this->report = &$this->_data['Report'];
			}
			else
				$this->_createSample();
		}

		public function setData( $data, $autosave = false )
		{
			if(is_array($data))
				$this->_data = $data;
			else
				$this->_data = json_decode(stripslashes($data), true);

			if($autosave)
				return $this->save();
			return $this->_validObject();
		}

		private function _validObject()
		{
			return !empty($this->_data);
		}

		public function _empty()
		{
			return empty($this->_data['Items']);
		}

		public function calc( $target )
		{
			$count = 0;
			for( $i = 0; $i < $this->count(); $i++ )
			{
				$item = $this->at($i);
				$count += count($item->$target);
			}

			return $count;
		}

		public function count( $corrent_only = false )
		{
			if( !$corrent_only ) return count($this->_data['Items']);
			
			$count_correct = 0;
			for( $i = 0; $i < $this->count(); $i++ )
			{
				$item = $this->at($i);
				if($item->validate())
					$count_correct++;
			}
			return $count_correct;
		}

		public function at( $idx = -1 )
		{
			if($this->_empty()) return false;

			if($idx == -1) $idx = array_rand($this->_data['Items']);

			if(!empty($this->_data['Items'][$idx]))
				return new EXTITEM($this->_data['Items'][$idx]);
		}

		public function validateRandomItem()
		{
			$result = false;
			if(!$this->_empty())
			{
				$item = $this->at();
				$result = $item->validate();

				if(empty($this->report)) $result = false;
			}

			return $result;
		}

		private function _createSample()
		{
			$this->_data = array('Items' => array());
			$this->save();
		}

		public function save()
		{
			return $this->_validObject() ? file_put_contents($this->_filename, stripslashes(json_encode($this->_data))) : false;
		}
	}
?>