<?php

	class blacklist
	{
		private $_items = array('useragent' => array(), 'source' => array());
		function __construct()
		{
			$tmp_blacklist = json_decode(get_option('extswa_blacklist', '[]'), true);
	        foreach($tmp_blacklist as $item)
	            $this->_items[$item['type']][] = $item['substr'];
		}

		function inBlacklist( $string, $type )
		{
			foreach($this->_items[$type] as $item)
			{
				if(strpos($string, $item) !== false)
					return true;
			}
			return false;
		}
	}
?>