<?php
function parse_referrer( $referrer )
{
	$parts = explode('/', $referrer);
	if( stripos($parts[2], 'yandex.') !== false )      $se = 'yandex';
	elseif(stripos($parts[2], 'google.') !== false )   $se = 'google';
	elseif(stripos($parts[2], 'nova.rambler.ru') !== false )  $se = 'rambler';
	elseif(stripos($parts[2], 'go.mail.ru') !== false )     $se = 'mail';
	elseif(stripos($parts[2], 'search.yahoo.') !== false )  $se = 'yahoo';
	elseif(stripos($parts[2], 'search.qip.ru') !== false )     $se = 'qip';
	elseif(stripos($parts[2], 'nigma.ru') !== false )  $se = 'nigma';
	elseif(stripos($parts[2], 'bing.com') !== false )     $se = 'bing';
	else    									   	  $se = 'custom';

	if($se != 'custom')
	{
		switch($se)
		{
			case 'yandex':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = iconv('cp1251','utf8',$q['text']);
			break;
			case 'google':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['q'];
			break;
			case 'bing':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['q'];
			break;
			case 'rambler':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['query'];
			break;
			case 'qip':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['query'];
			break;
			case 'mail':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['q'];
			break;
			case 'yahoo':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['p'];
			break;
			case 'nigma':
				$ar = parse_url($referrer);
				parse_str($ar['query'], $q);
				$query = $q['s'];
			break;
			default:
				$query = $referrer;
			break;
		}

		return array('se' => $se, 'query' => $query);
	}
	return false;
}

function isAvailableSE( $se )
{
	$_available = explode(',', EXTSWA_AVAILABLE_SE);
	return in_array($se, $_available);
}

function getRandomSE($ignore = array())
{
	$array = array_diff(getEnabledSE(), $ignore);
	foreach($array as $name)
	{
		if(mt_rand(0,999) < getPercentSE($name) * 10)
			return $name;
	}
	
	return $array[array_rand($array)];
}

function getAvailableSE( $ignore = array())
{
	return array_diff(explode(',', EXTSWA_AVAILABLE_SE), $ignore);
}

function getEnabledSE()
{
	return explode(',', EXTSWA_ENABLED_SE);
}

function getNamesSE()
{
	return array(	
					'google' => 'Google',
					'yandex' => 'Yandex',
					'rambler' => 'Rambler',
					'mail' => 'Mail',
					'bing' => 'Bing',
					'yahoo' => 'Yahoo',
					'qip' => 'QiP',
					'nigma' => 'Nigma',
					'custom' => 'Ссылка'
				);
}

function seToString( $se_name )
{
	$se = getNamesSE();
	return array_key_exists($se_name, $se) ? $se[$se_name] : $se_name;
}

function getPercentSE( $se = false )
{
	$array = json_decode(get_option('extswa_percent_se', EXTSWA_DEFAULT_PERCENT_SE), true);
	return $se ? ( array_key_exists($se, $array) ? $array[$se] : 10 ) : $array;
}
?>