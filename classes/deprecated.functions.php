<?php
function reftostr($referrer)
{
    $ret = parse_referrer($referrer);
    return $ret ? implode(':', array_values($ret)) : false;
}

function setYandexLr( $referrer, $country )
{
    $ret = parse_referrer($referrer);
    if(!empty($ret['se']) && $ret['se'] == 'yandex')
        return $referrer . '&lr=' . yandexreg($country);
    return $referrer;
}

function alert( $message, $error = false )
{
    ?><div class="<?php echo !$error ? 'updated' : 'error';?>"><p><?php echo $message; ?></p></div><?php
}

function yandexreg($country)
{
    $regions = array(
         'by' => array( '149', '157', '29629', '29634', '29632', '29633', '29631', '29630' ),
         'ru' => array( '225', '213', '2', '214', '215', '216', '217', '219', '10740', '10738', '10743', '10747', '63', '65' ),
         'de' => array( '177', '178', '99', '100', '98' ),
         'it' => array( '205' ),
         'kz' => array( '159' ),
         'ge' => array( '169' ),
         'ua' => array( '20544', '20549', '20546', '20545', '20541', '20536', '20529' ),
         'md' => array( '10313', '10314', '10315' ),
         'az' => array( '167' ),
         'am' => array( '168' ),
         'il' => array( '181' ),
         'lv' => array( '206' ),
         'un' => array( '111', '166', '958' ),
         'lt' => array( '117' ),
         'tj' => array( '209' ),
         'tm' => array( '170' ),
         'uz' => array( '171' ),
         'fr' => array( '124' ),
         'ee' => array( '179' )
     );

    $country = array_key_exists(strtolower($country), $regions) ? strtolower($country) : 'un';
    return $regions[$country][array_rand($regions[$country])];
}

function dellr($url)
{
    $ar = parse_url($url);
    parse_str($ar['query'], $q);
    if (stripos($url, 'yandex') !== false)
    {
        return $ar['scheme'] . '://yandex.ru' . $ar['path'] . '?text=' . urlencode($q['text']);
    }
    else
        return $url;
}

function rand_lvl( $lvl = false )
{
    $key = $lvl ? 'lvlafter' : 'lvl';
    $lvl = empty($_REQUEST[$key]) ? '' : trim($_REQUEST[$key]);

    switch ($lvl)
    {
        case '2t5':
            return rand(2, 5);
        break;
        case '2t9':
            return rand(2, 9);
        break;
        case '2t13':
            return rand(2, 13);
        break;
        default:
            return rand(2, 5);
        break;
    }
}

function getPages()
{
    global $wpdb;
    $urls = array();

    $rows = $wpdb->get_results("SELECT id, post_date FROM {$wpdb->prefix}posts WHERE post_status='publish' OR post_status='inherit'", 'ARRAY_A');
    foreach($rows as $row)
    {
        $urls[] = str_replace( EXTSWA_SITE_ROOT_URL, '', get_permalink( $row['id'] ) );

        $calendar_url = '/?m=' . str_replace('-', '', @reset(explode(' ', $row['post_date'])));
        if(!in_array($calendar_url, $urls))
            $urls[] = $calendar_url;
    }

    $rows = $wpdb->get_results("SELECT term_id FROM {$wpdb->prefix}term_taxonomy WHERE taxonomy='category'", 'ARRAY_A');
    foreach($rows as $row)
        $urls[] = '/?cat=' . $row['term_id'];

    return $urls;
}
?>