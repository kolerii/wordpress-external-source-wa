<?php

class referrer
{
	static public function belongs_country( $host, $country )
	{
		$zone = @end(explode('.',$host));
		switch ($country)
		{
			case 'BY':
				if(in_array($zone, array('ua'))) return false;
			break;
			case 'RU':
				if(in_array($zone, array('ua','by'))) return false;
			break;
			case 'UA':
				if(strpos($host, 'yandex') !== false)
					if(in_array($zone, array('by','ru','kz'))) return false;
				else
					if(in_array($zone, array('by','kz'))) return false;
			break;
			default:
				return true;
			break;
		}
		return true;
	}

	static public function yandex( $query , $host = false, $page = false, $country = false)
	{
		if(empty($host)) $host = 'yandex.ru';

		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = 'redircnt='.rand(100000000,1000000000).'.'.rand(0,9);
		else
			$page = 'p=' . ($page-1);

		$lr = $country ? '&lr='.yandexreg($country) : '';

		$query = urlencode( $query );

		return "http://{$host}/yandsearch?rdrnd=".rand(100000,999999)."&text={$query}{$lr}&{$page}";
	}

	static public function bing( $query , $host = false, $page = false, $country = false)
	{
		if(empty($host)) $host = 'bing.com';

		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
		{
			$form = '&form=QBLH';
			$page = '';
		}
		else
		{
			$form = '&FORM=PERE' . $page;
			$page = '&first=' . ($page - 1) . 1;
		}

		return "http://{$host}/search?q={$query}&qs=n&pq={$query}&sc=".rand(0,9)."-".rand(0,9)."&sp=-1&sk={$page}{$form}";
	}

	static public function custom( $url, $host = false, $page = false, $country = false)
	{
		return $url;
	}

	static public function google( $query, $host = false, $page = false , $country = false)
	{
		if(empty($host)) $host = 'google.com';

		$query = urlencode($query);
		$page--;
		$page = (empty($page) || $page == 1 ) ? '' : "&start={$page}0";
		return "https://{$host}/#q={$query}{$page}";
	}

	static public function nigma( $query, $host = false, $page = false , $country = false)
	{
		$query = urlencode($query);
		$page--;
		$page = (empty($page) || $page == 1 ) ? '' : "&startpos={$page}0";
		return "http://www.nigma.ru/index.php?s={$query}{$page}";
	}

	static public function yahoo( $query, $host = false, $page = false , $country = false)
	{
		$query = urlencode($query);
		$page--;
		$page = (empty($page) || $page == 1) ? '' : "&b={$page}1";
		return "https://search.yahoo.com/search;?p={$query}ei=UTF-8{$page}"; 
	}

	static public function rambler( $query, $host = false, $page = false, $country = false)
	{
		if(empty($host)) $host = 'rambler.ru';

		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = '';
		else
			$page = '&page=' . $page;

		return "http://{$host}/search?btnG=%D0%9D%D0%B0%D0%B9%D1%82%D0%B8!&query={$query}{$page}";
	}

	static public function search( $query, $host = false, $page = false, $country = false)
	{
		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = '';
		else
			$page = '&nav=' . $page . '.10.2.10';

		return "http://search.com/search?q={$query}{$page}";
	}	

	static public function metabot( $query, $host = false, $page = false, $country = false)
	{
		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = '&wd=0';
		else
			$page = '&ynp=0&rbp=0&atp=0&app=0&epp=0&rmp=0&ggp=0&gmp=0&arp=0&mrp=0&grp=20&yhp=21&bn=3&bt=1&ft=17&fn='.($page - 1).'7&gp='.$page;

		return "http://metabot.ru/?st={$query}{$page}";
	}

	static public function liveinternet( $query, $host = false, $page = false, $country = false)
	{
		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = '';
		else
			$page = '&t=web&p='.$page;

		return "http://www.liveinternet.ru/q/?q={$query}{$page}";
	}

	static public function mamma( $query, $host = false, $page = false, $country = false)
	{
		$query = urlencode( $query );

		return "http://mamma.com/result.php?type=web&q={$query}&j_q=&l=";
	}

	static public function webalta( $query, $host = false, $page = false, $country = false)
	{
		$query = urlencode( $query );
		if($page !== false && (int)$page <= 0)
			$page = false;
		if((empty($page) || $page == 1 ))
			$page = '';
		else
			$page = '&page='.$page;

		return "http://webalta.ru/search?q={$query}{$page}";
	}

	static public function qip( $query, $host = false, $page = false, $country = false)
	{
		$sort = '';
		if($page !== false && (int)$page <= 0)
			$page = false;
		$query = urlencode( $query );
		if((empty($page) || $page == 1 ))
			$page = '';
		else
			$sort = '&sort=rlv';

		return "http://qip.ru/search/{$page}?query={$query}{$sort}";
	}

	static public function tut( $query, $host = false, $page = false, $country = false)
	{
		$str = '';
		if($page !== false && (int)$page <= 0)
			$page = false;
		$query = urlencode( $query );
		if((empty($page) || $page == 1 ))
		{
			$status = 'status=1&';
			$page = '0';
		}
		else
		{
			$status = 'rs=1&';
			$page = $page - 1;
			$str = '&tc=0&ust=' . $query . '&sh=&cg=15&cdig=1';
		}

		return "http://tut.by/?{$status}ru=1&encoding=1&page={$page}&how=rlv&query={$query}{$str}";
	}

	static public function mail( $query, $host = false, $page = false, $country = false)
	{
		$str = '';
		if($page !== false && (int)$page <= 0)
			$page = false;
		$query = urlencode( $query );
		if((empty($page) || $page == 1 ))
		{
			$status = 'mailru=1&';
			$page = '';
		}
		else
		{
			$status = '';
			$page = '&where=any&num=10&rch=e&sf='. ($page - 1) . 0;
		}

		return "http://mail.ru/search?{$status}q={$query}{$page}";
	}
}

?>
