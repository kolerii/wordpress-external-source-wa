<?php
    require '__CONSTANTS.php';

    global $wpdb;
    if($settings = json_decode(get_option('extswa_settings'), true))
    {
        $_TYPES_ENABLED_GENERATION = explode(',', @$settings['typegen']);
        if(in_array(STATIC_GENERATION, $_TYPES_ENABLED_GENERATION))
        {
        	$wpdb->query('INSERT INTO '.EXTSWA_DB_LOG.' (`result`) VALUES (`1`)');
            @file_get_contents( EXTSWA_AJAX_URL . "?action=generate_extsource_file&timer={$settings['timer']}&ignor={$settings['ignor']}&source={$settings['source']}&lvl={$settings['lvl']}&lvl={$settings['lvlafter']}&real={$settings['real']}");
        }
    }
?>