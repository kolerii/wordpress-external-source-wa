<?php

require_once CLASS_GEO;
require_once CLASS_UserAgent;
require_once LIBRARY_SE;

if(empty($_SERVER['HTTP_CF_IPCOUNTRY']))
{
    $reg = new SxGeo(SXFILELIB);
    $_COUNTRY = $reg->getCountry($_SERVER['REMOTE_ADDR']);
}
else
    $_COUNTRY = $_SERVER['HTTP_CF_IPCOUNTRY'];

$browser = new UserAgent();

$_REQUEST_URI     = mysql_real_escape_string($_SERVER['REQUEST_URI']);
$_HTTP_REFERER    = empty($_SERVER['HTTP_REFERER']) ? '' : mysql_real_escape_string($_SERVER['HTTP_REFERER']);
$_HTTP_USER_AGENT = mysql_real_escape_string($_SERVER['HTTP_USER_AGENT']);

$browser->setUserAgent($_HTTP_USER_AGENT);

$struct_browser = $browser->getBrowser() . '/' . $browser->getVersion() . '/' . $browser->getPlatform();
if(!strstr($struct_browser, 'unknown') && $browser->isRobot() == false )
{
	$count = $wpdb->get_var("SELECT COUNT(`data`) FROM ".EXTSWA_DB_STATS." WHERE `data`='$struct_browser' AND `date`='".EXTSWA_DATE."'");
	if($count)
	{
		$links = $wpdb->get_var("SELECT `links` FROM ".EXTSWA_DB_STATS." WHERE `data`='$struct_browser' AND `date`='".EXTSWA_DATE."' LIMIT 1");
		$json = json_decode($links, true);

		if(!in_array($_REQUEST_URI, $json ))
			$json[] = $_REQUEST_URI;

		$wpdb->query("UPDATE ".EXTSWA_DB_STATS." SET `count`=`count`+1, `links`='".json_encode($json)."' WHERE `data`='$struct_browser' AND `date`='".EXTSWA_DATE."'");
	}
	else
		$wpdb->query("INSERT INTO ".EXTSWA_DB_STATS." (`type`,`date`,`data`,`count`,`source`,`links`) VALUES ('0','".EXTSWA_DATE."','$struct_browser','1','".$_HTTP_USER_AGENT."','".json_encode(array($_REQUEST_URI))."')");
}

if(!empty($_HTTP_REFERER))
{
	$parsed_referrer = parse_referrer($_HTTP_REFERER);
	if($parsed_referrer)
	{
		if($parsed_referrer['query'] != $_SERVER['HTTP_HOST'])
		{
			$struct_referrer = $parsed_referrer['se'].'/'.$parsed_referrer['query'].'/'.$_COUNTRY;
			if($wpdb->get_var("SELECT COUNT(`data`) FROM ".EXTSWA_DB_STATS." WHERE `data`='$struct_referrer' AND `date`='".EXTSWA_DATE."'"))
			{
				$links = $wpdb->get_var("SELECT `links` FROM ".EXTSWA_DB_STATS." WHERE `data`='$struct_referrer' AND `date`='".EXTSWA_DATE."' LIMIT 1");
				$jsonz = json_decode($links, true);

				if(!in_array($_REQUEST_URI, $jsonz))
					$jsonz[] = $_REQUEST_URI;

				$wpdb->query("UPDATE ".EXTSWA_DB_STATS." SET `count`=`count`+1, `links`='".json_encode($jsonz)."' WHERE `data`='$struct_referrer' AND `date`='".EXTSWA_DATE."'");
			}
			else
				$wpdb->query("INSERT INTO ".EXTSWA_DB_STATS." (`type`,`date`,`data`,`count`,`source`,`links`) VALUES ('1','".EXTSWA_DATE."','$struct_referrer','1','".$_HTTP_REFERER."','".json_encode(array($_REQUEST_URI))."')");
		}
	}
}
?>
