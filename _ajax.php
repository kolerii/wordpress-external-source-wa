<?php

require_once '__CONSTANTS.php';

require_once LIBRARY_SE;
require_once CLASS_Referrer;
require_once CLASS_Extfile;
require_once CLASS_Statistic;
require_once CLASS_Blacklist;

require_once LIBRARY_DEPRECATED_FUNCTIONS;

$_EXT = new EXTFILE( EXTSWA_EXTFILE_PATH );

$constants = array('ignor' => 'USE_EXMASKS', 'filename' => 'NEW_FILENAME', 'source' => 'SOURCE_REFERRERS', 'filepass' => 'FILEPASS', 'real' => 'STATUS_MODE_REAL', 'action' => 'ACTION', 'timer' => 'RANGE_STATISTICS', 'lvl' => 'DEPTH_VIEW_BEFORE', 'lvlafter' => 'DEPTH_VIEW_AFTER', 'autogen' => 'ENABLED_AUTO_GENERATION' );

if(!empty($_REQUEST['filename'])) $_REQUEST['filename'] = rtrim(str_replace('/','',$_REQUEST['filename']), '.txt');

foreach($constants as $key => $name)
	define($name, empty($_REQUEST[$key]) ? '' : trim(strtolower($_REQUEST[$key])));

$_TYPES_ENABLED_GENERATION = empty($_REQUEST['typegen']) ? array() : explode(',', trim($_REQUEST['typegen']));

if(EXTSWA_AJAXPASS != trim($_REQUEST['_cajax'])) die( 'Access denied!' );

switch(ACTION)
{
	case 'change_settings':
        $json = json_decode(get_option('extswa_settings'), true);
        
        $json['ignor']   = USE_EXMASKS;
        $json['real']    = STATUS_MODE_REAL;
        $json['timer']   = RANGE_STATISTICS;
        $json['lvl']     = DEPTH_VIEW_BEFORE;
        $json['lvlafter']= DEPTH_VIEW_AFTER;
        $json['typegen'] = implode(',',$_TYPES_ENABLED_GENERATION);
        $json['autogen'] = ENABLED_AUTO_GENERATION;

        if(NEW_FILENAME . '.txt' != rtrim(EXTSWA_EXTFILE_NAME, '.txt'))
        {
        	update_option('extswa_file', NEW_FILENAME . '.txt');
            if (!rename(EXTSWA_EXTFILE_PATH, NEW_FILENAME . '.txt'))
            {
                unlink(EXTSWA_EXTFILE_PATH);
                file_put_contents(NEW_FILENAME . '.txt', '');
            }
        }

        $enabled_se = array();
        $set_se = empty($_REQUEST['enabled_se']) ? array() : explode(',',$_REQUEST['enabled_se']);
        $available = explode(',', EXTSWA_AVAILABLE_SE);

        foreach($set_se as $se)
        {
            if(isAvailableSE($se))
                $enabled_se[] = $se;
        }

        update_option('extswa_enabled_se', implode(',',$enabled_se));
        update_option('extswa_percent_se', json_encode($_POST['percent']));

        if (FILEPASS !== false && empty($json['filepass']))
            update_option('extswa_ajaxpass', md5(FILEPASS));

        $json['source'] = SOURCE_REFERRERS;
        update_option('extswa_settings', json_encode($json));
        die(alert('Настройки успешно сохранены!'));
	break;
	case 'save_list_psereferrer':
        if(empty($_REQUEST['data']))
            alert('Сохранение невозможно, недостаточное количество созданных фейковых рефереров!', true);
        else
        {
            if(json_decode($_REQUEST['data'], true))
            {
                update_option('extswa_psereferrers', trim($_REQUEST['data']));
                die( alert('Список успешно сохранен!') );
            }

            die( alert('При сохранении произошла ошибка!', true) );        
        }
	break;
    case 'generate_psereferrers_titles':
        $return = array();
        $answer = $wpdb->get_results('SELECT ID,post_title FROM ' . $wpdb->prefix . 'posts WHERE post_status="publish"','ARRAY_A');
        foreach($answer as $row)
            $return[] = array( 'se' => getRandomSE(array('custom')), 'page' => str_replace(EXTSWA_SITE_ROOT_URL, '', get_permalink($row['ID'])), 'query' => mysql_real_escape_string($row['post_title']));

        die(json_encode($return));
    break;
    case 'generate_psereferrers_topmail':
        require LIBRARY_SHD;
        require CLASS_Browser;

        $pages = statistic::getArrayPages();

        $id = (float)$_POST['id'];
        $browser = new browser(true,true);
        $browser->setRef('http://top.mail.ru/');
        $dom = str_get_html($browser->go('http://top.mail.ru/keywords.html?id=' . $id . '&period=0&date=&sf=0&pp=200&gender=0&agegroup=0&searcher=merge&'));
        if(is_object($dom))
        {
            $ret = $dom->find('td.it-string');
            if(!empty($ret))
            {
                $return = array();
                foreach( $ret as $td )
                    $return[] = array('query' => trim($td->plaintext), 'se' => getRandomSE(array('custom')), 'page' => $pages[array_rand($pages)]);
            }
            $dom->clear();
        }
        $browser = null;
        die(json_encode($return));
    break;
    case 'generate_psereferrers_vk':
        $return = array();
        $pages = statistic::getArrayPages();
        $max = (int)$_POST['max'];

        $url = 'http://vk.com/search?c%5Bq%5D='.urlencode($_POST['query']).'&c%5Bsection%5D=communities';
        $results = file_get_contents($url);
        preg_match_all('#<a href="([\/_\d\w.]{1,})" class="simple_fit_item search_item">#', $results, $tmp);
        if(!empty( $tmp[1] ))
        {
            foreach($tmp[1] as $url)
                $return[] = array( 'query' => 'http://vk.com'.$url, 'se' => 'custom', 'page' => $pages[array_rand($pages)]);
        }

        die(json_encode(array_slice($return, 0, $max)));
    break;
	case 'generate_psereferrers_contents':
        $return = array();
        $answer = $wpdb->get_results('SELECT ID,post_content FROM ' . $wpdb->prefix . 'posts WHERE post_status="publish"','ARRAY_A');
        foreach($answer as $row)
        {
        	$max = ($_REQUEST['max'] <= 0) ? 1 : (int)$_REQUEST['max'];
        	$min = ($_REQUEST['min'] < 0)  ? 0 : (int)$_REQUEST['min'];

        	$count_referrers = rand($min, $max);

            $content = strip_tags(preg_replace(array('#\[(?:/|)cc[\s\w\d\"\=\_]{1,}\]#', "#(?:\r|\n)#",'#(?:[\s]{2,}|\t)#'), array('','',' '), $row['post_content']));

            $words = explode(' ', $content);

        	for( $i = 0; $i < $count_referrers; $i++ )
        	{
                $request = implode(' ', array_slice($words, array_rand($words), rand( 4, 7 )));

                if(strlen($request) > 10)
                    $return[] = array('se' => getRandomSE(array('custom')), 'page' => str_replace(EXTSWA_SITE_ROOT_URL, '', get_permalink($row['ID'])), 'query' => mysql_real_escape_string(trim(stripslashes($request))));
        	}
        }
        die(json_encode($return));
	break;
    case 'save_exmasks':
        if(json_decode($_POST['data'],true))
        {
            update_option('extswa_exmasks', trim($_POST['data']));
            die( alert('Сохранение произошло успешно!') );
        }
        else
            die( alert('Произошла ошибка при сохранении!', true) );
    break;
    case 'get_table_exmasks':
        $data = json_decode(get_option('extswa_exmasks','[]'));
        ?>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Маска</th>
                </tr>
            </thead>
            <tbody class="exmasks-tbody"><?php
                $count = count($data);
                for ($i = 0; $i < $count; $i++):?>
                    <tr>
                        <td><input type="button" value="х" class="exmasks-radio"></td>
                        <td><input type="text" class="exmasks-input" value="<?=$data[$i]?>"></td>
                    </tr>
                <?php endfor;
            ?></tbody>
        </table><?php
    break;
    case 'get_list_psereferrers':
        $json = json_decode(get_option('extswa_psereferrers','[]'), true);
        $return = array();
        if (!empty($json))
        {
            foreach($json as $array)
                $return[] = array('se' => $array['se'], 'page' => $array['url'], 'query' => trim(str_replace('\r\n','',stripslashes(urldecode($array['referer'])))));
        }
        die(json_encode($return));
    break;
    case 'show_table_statistic':
        if (!empty($_REQUEST['q']))
        {
            $variant_stats = array('BROWSER' => 0, 'SEARCH' => 1, 'F' => 2, 'R' => 3);
            if(array_key_exists($_REQUEST['q'], $variant_stats))
            {
                $variant_key = $variant_stats[$_REQUEST['q']];

                if(in_array($variant_key, array(0,1,2)))
                    $statistic = $wpdb->get_results("SELECT `count`,`source`,`data` FROM " . EXTSWA_DB_STATS . " WHERE `date`='" . EXTSWA_DATE . "' AND `type`=$variant_key ORDER BY `count` DESC", 'ARRAY_A');

                switch($variant_key)
                {
                    case 3:
                        ?><table class="wp-list-table widefat fixed posts"><thead><tr><td>Реферер</td><td>Количество</td></tr></thead><tbody><?php

                        $statistic = statistic::getRealReferrers();
                        $statistic_count = count($statistic['count']);
                        for($i = 0; $i < $statistic_count; $i++)
                            if ($statistic['count'][$i] >= 1)
                                echo '<tr><td>', $statistic['ref'][$i], '</td><td>', $statistic['count'][$i], '</td></tr>';
                    break;
                    case 1:
                        ?><table class="wp-list-table widefat fixed posts"><thead><tr><td>Поисковая система</td><td>Запрос</td><td>Страна</td><td>Количество</td></tr></thead><tbody><?php
                            foreach($statistic as $row)
                            {
                                $country = explode('/', $row['data']);
                                $parsed_referrer = parse_referrer($row['source']);
                                if($parsed_referrer['se'] == 'custom') $parsed_referrer['se'] = 'Ссылка';
                                echo '<tr><td>' , $parsed_referrer['se'] , '</td><td>' , !empty($parsed_referrer['query']) ? $parsed_referrer['query'] : '<Запрос не найден>' , '</td><td>' , $country[2] , '</td><td>' , $row['count'] , '</td></tr>';
                            }
                    break;
                    case 0:
                        ?><table class="wp-list-table widefat fixed posts"><thead><tr><td>Браузер</td><td>Версия</td><td>ОС</td><td>Количество</td></tr></thead><tbody><?php
                            foreach($statistic as $row)
                            {
                                $browser = explode('/', $row['data']);
                                $parsed_referrer = parse_referrer($row['source']);
                                echo '<tr><td>' , $browser[0] , '</td><td>' , $browser[1] , '</td><td>' , $browser[2] , '</td><td>' , $row['count'] , '</td></tr>';
                            }
                    break;
                    case 2:
                        ?><table class="wp-list-table widefat fixed posts"><thead><td>Тип</td><td>UserAgent</td><td>Реферер</td><td>Страница</td><td>Маска</td><td>Количество</td></thead><tbody><?php
                            foreach($statistic as $row)
                            {
                                $report = json_decode($row['source'], true);
                                $parsed_referrer = parse_referrer($report['referrer']);

                                echo '<tr><td>',($report['type'] == 0 ? 'Просмотр' : 'Переход'),'</td><td>', $report['ua'], '</td><td>', $parsed_referrer['se'], ':', $parsed_referrer['query'], '</td><td>', $report['page'], '</td><td>', $report['mask'], '</td><td>', $row['count'], '</td></tr>';
                            }
                    break;
                }

                ?></tbody></table><?php
            }
        }
    break;
    case 'generate_extsource_file':
        $settings = json_decode(get_option('extswa_settings'), true);
        $_TYPES_ENABLED_GENERATION = explode(',', $settings['typegen']);

        $_blacklist = new blacklist;

        if(!empty($_TYPES_ENABLED_GENERATION) && in_array(STATIC_GENERATION, $_TYPES_ENABLED_GENERATION))
        {
            $_used = array('referrers' => array(), 'useragents' => array());
            $useragents = statistic::getUserAgents();
            $useragents = array_merge($useragents,json_decode(get_option('extswa_useragents','[]'),true));

            if(in_array(SOURCE_REFERRERS, array('all','stats')))
            {
                switch (RANGE_STATISTICS)
                {
                    case 'today':
                        $osk = 'AND `date`="'.EXTSWA_DATE.'"';
                    break;
                    case '1dl':
                        $osk = 'AND (`date`="'.EXTSWA_DATE.'" OR `date`="' . date('d.m.Y', strtotime('-1 day')) . '")';
                    break;
                    case '7dl':
                        $osk = 'AND (`date`="'.EXTSWA_DATE.'"';
                        for( $i = 1; $i < 7; $i++ )
                            $osk .= ' OR `date` = "' .date('d.m.Y', strtotime("-{$i} day")). '"';
                        $osk .= ')';
                    break;
                    case 'all':
                        $osk = '';
                    break;
                    default:
                        $osk = 'AND `date`="'.EXTSWA_DATE.'"';
                    break;
                }

                $a      = array();

                $answer = $wpdb->get_results('SELECT * FROM ' . EXTSWA_DB_STATS . " WHERE `type`=1 $osk AND `links`!=''", 'ARRAY_A');
                foreach( $answer as $row )
                {
                    $json = json_decode($row['links'], true);
                    for( $i = 0; $i < count($json); $i++ )
                    {
                        $a[$json[$i]][] = $row['source'];
                        $countarr[]     = $row['count'];
                    }
                }
                    
                if(STATUS_MODE_REAL == ENABLED || STATUS_MODE_REAL == SINGLE_PASS)
                    $reports = statistic::getReportsReferrers($osk);

                foreach($a as $key_link => $referrer)
                {
                    $_used['UserAgents'] = array();
                    $item_array = array('Pages' => array(), 'Referers' => array(), 'UserAgents' => array(), 'Paths' => array(), 'AfterPaths' => array());
                    $count_referrers = count($a[$key_link]);

                    $item_array['Pages'][] = array( 'Page' => $key_link, 'Priority' => rand(1, count($a) * 2));
                    for( $i = 0; $i < $count_referrers; $i++ )
                    {
                        $without_lr = dellr($a[$key_link][$i]);

                        if($_blacklist->inBlacklist($without_lr,'source')) continue;

                        if(!in_array($without_lr, $_used['referrers']))
                        {
                            $_used['referrers'][] = $without_lr;

                            $referrer = array('Referer' => $without_lr, 'Priority' => $countarr[$i]);

                            if(STATUS_MODE_REAL == ENABLED || STATUS_MODE_REAL == SINGLE_PASS)
                            {
                                $struct_referrer = reftostr($referrer['Referer']);
                                if(in_array($struct_referrer, $reports['ref']))
                                    $referrer['Priority'] -= $history['count'][array_search($struct_referrer, $reports['ref'])];
                            }

                            if($referrer['Priority'])
                                $item_array['Referers'][] = $referrer;
                        }
                    }

                    //SINGLE_PASS
                    if(empty($item_array['Referers']) && STATUS_MODE_REAL == SINGLE_PASS)
                        $item_array['Referers'][] = array('Priority' => 1, 'Referer' => $_used['referrers'][array_rand($_used['referrers'])]);

                    for($i = 0; $i < rand(4, 5); $i++)
                        $item_array['Paths'][]   = array('Path' => array_fill(0, rand_lvl(), '/'), 'Priority' => rand(1,300));

                    for($i = 0; $i < rand(4, 5); $i++)
                        $item_array['AfterPaths'][]   = array('Path' => array_fill(0, rand_lvl(true), '/'), 'Priority' => rand(1,300));

                    for ($i = 0; $i < rand(4, 5); $i++)
                    {
                        $useragent = @$useragents[array_rand($useragents)];
                        if (!in_array($useragent, $_used['useragents']) && !empty($useragent))
                        {
                            $_used['useragents'][] = $useragent;
                            if($_blacklist->inBlacklist($useragent,'useragent')) continue;
                            $item_array['UserAgents'][] = array( 'UserAgent' => $useragent, 'Priority' => rand(1, count($useragent)));
                        }
                    }

                    if(!empty($item_array['Pages']) && !empty($item_array['Referers']) && !empty($item_array['UserAgents']))
                        $Item['Items'][] = $item_array;
                }

            }

            if(in_array(SOURCE_REFERRERS, array('pse','all')))
            {
                $psereferrers = json_decode(get_option('extswa_psereferrers','[]'), true);
                $count_psereferrers = count($psereferrers);

                for($i = 0; $i < $count_psereferrers; $i++)
                {
                    $_used['UserAgents'] = array();
                    if(!isAvailableSE($psereferrers[$i]['se']) && $psereferrers[$i]['se'] == 'custom')
                        continue;
                    elseif(!isAvailableSE($psereferrers[$i]['se']))
                        $psereferrers[$i]['se'] = getRandomSE();

                    $item_array = array('Pages' => array(), 'Referers' => array(), 'UserAgents' => array(), 'Paths' => array(), 'AfterPaths' => array());
                    $item_array['Pages'][] = array('Page' => $psereferrers[$i]['url'], 'Priority' => rand(1, $count_psereferrers * 2));

                    $generated_referer = referrer::$psereferrers[$i]['se']($psereferrers[$i]['referer'], false, rand(0,5));
                    if($_blacklist->inBlacklist($generated_referer,'source')) continue;
                    $item_array['Referers'][] = array('Referer' => $generated_referer, 'Priority' => rand(1, $count_psereferrers * 2));

                    for ($j = 0; $j < rand(4, 5); $j++)
                        $item_array['Paths'][]   = array('Path' => array_fill(0, rand_lvl(), '/'), 'Priority' => rand(1,300));

                    for($j = 0; $j < rand(4, 5); $j++)
                        $item_array['AfterPaths'][]   = array('Path' => array_fill(0, rand_lvl(true), '/'), 'Priority' => rand(1,300));
                        
                    for ($j = 0; $j < rand(4, 5); $j++)
                    {
                        $useragent = @$useragents[array_rand($useragents)];
                        if (!in_array($useragent, $_used['useragents']) && !empty($useragent))
                        {
                            $_used['useragents'][] = $useragent;
                            if($_blacklist->inBlacklist($useragent,'useragent')) continue;
                            $item_array['UserAgents'][] = array( 'UserAgent' => $useragent, 'Priority' => rand(1, count($useragent)));
                        }
                    }

                    if(!empty($item_array['Pages']) && !empty($item_array['Referers']) && !empty($item_array['UserAgents']))
                        $Item['Items'][] = $item_array;
                }
            }

            $Item['Report'] = EXTSWA_REPORT_SCRIPT_URL;
            if (USE_EXMASKS == ENABLED)
            {
                if($exmasks = json_decode(get_option('extswa_exmasks','[]'), true))
                    $Item['ExMasks'] = $exmasks;
            }

            $checkpages = json_decode(get_option('extswa_checkpages', '[]'), true);
            if(!empty($checkpages))
                $Item['CheckPages'] = $checkpages;

            foreach(array('extswa_extensions' => 'Extensions', 'extswa_exextensions' => 'ExExtensions', 'extswa_mimetypes' => 'MimeTypes', 'extswa_exmimetypes' => 'ExMimeTypes', 'extswa_urlsfilters' => 'URLFilters') as $option => $key)
                $Item[$key] = json_decode(get_option($option,'[]'), true);

            if ($_EXT->setData($Item, true))
            {
                echo 'Файл Внешнего Источника успешно сгенерирован.<br> Файл доступен по ссылке: ', EXTSWA_EXTFILE_URL, '<hr><br>Автоматическая валидация: ';
                $_extfile = new EXTFILE(EXTSWA_EXTFILE_URL);
                if($_extfile->validateRandomItem())
                    alert('Созданный файл Внешнего Источника успешно прошел проверку.');
                else
                    alert('Созданный файл Внешнего Источника не прошел проверку.', true);
            }
            else
                alert('При генерации произошла ошибка, проверьте, пожалуйста, права доступа и достаточное наличие собранных данных!', true);
        }
    break;
    case 'save_lists_filters':
        foreach(array('extswa_extensions' => 'ext', 'extswa_exextensions' => 'exext', 'extswa_mimetypes' => 'mime', 'extswa_exmimetypes' => 'exmime', 'extswa_urlsfilters' => 'urlsfilters') as $option => $key)
            update_option($option, json_encode(empty($_POST[$key]) ? array() : preg_split("/[\n\r]+/s",@$_POST[$key])));
        alert('Списки фильтров сохранены успешно!');
    break;
    case 'save_lists_useragents':
        update_option('extswa_useragents', json_encode(empty($_POST['useragents']) ? array() : preg_split("/[\n\r]+/s",@$_POST['useragents'])));
        alert('Список UserAgent`ов сохранен успешно!');
    break;  
    case 'save_list_checkpages':
        $json = json_decode($_POST['data'], true);
        if(!empty($json))
        {
            $data = array();
            foreach($json as $element)
            {
                if(empty($element['page'])) continue;

                $el = array();

                foreach(array('page' => 'Page', 'ip' => 'ExpectedIP', 'substr' => 'IncludedText', 'useragent' => 'UserAgent') as $key => $jsonkey)
                {
                    if(!empty($element[$key]))
                        $el[$jsonkey] = trim($element[$key]); 
                }

                $data[] = $el;
            }

            update_option('extswa_checkpages', json_encode($data));
            die( alert('Список успешно сохранен!') );
        }

        die( alert('При сохранении произошла ошибка!', true) );
    break;
    case 'save_blacklist':
        $json = json_decode($_POST['data'], true);
        if(!empty($json))
        {
            $data = array();
            foreach($json as $item)
            {
                if(!empty($item['substr']) && !empty($item['type']))
                    $data[] = $item;
            }
            update_option('extswa_blacklist', json_encode($data));
            die( alert('Список успешно сохранен!') );
        }
        die( alert('При сохранении произошла ошибка', true) );
    break;
    case 'validate_file':
        $result = false;
        if(!empty($_REQUEST['link']))
        {
            $_extfile = new EXTFILE($_REQUEST['link']);
            $result = $_extfile->validateRandomItem();
        }
        die( $result ? 'Success' : 'Error' );
    break;
}
?>