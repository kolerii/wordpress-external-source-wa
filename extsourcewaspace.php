<?
/*
    Plugin Name: External Source of Waspace
    Description: Плагин для сбора поисковых рефереров и формирования внешнего источника
    Version: 2.5.2
    Author: Беляков Владислав [kolerii]
    Author URI: http://kolerii.ru/
*/

global $wpdb;

require_once '__CONSTANTS.php';

register_activation_hook(__FILE__, 'extswa_plugin_activation');
register_deactivation_hook(__FILE__, 'extswa_plugin_deactivation');

add_action('admin_menu', 'extswa_admin_generate_menu');
add_action('wp_loaded', 'extswa_load_statistic');

function extswa_plugin_activation()
{
    global $wpdb;

    add_option('extswa_file', EXTSWA_DEFAULT_FILENAME);
    add_option('extswa_settings', '[]');
    add_option('extswa_exmasks', '[]');
    add_option('extswa_psereferrers','[]');
    add_option('extswa_checkpages','[]');

    add_option('extswa_exextensions', '[]');
    add_option('extswa_extensions', '[]');
    add_option('extswa_mimetypes', '[]');
    add_option('extswa_exmimetypes', '[]');
    add_option('extswa_blacklist', '[]');
    add_option('extswa_useragents', '[]');

    add_option('extswa_ajaxpass', EXTSWA_DEFAULT_AJAXPASS);

    add_option('extswa_enabled_se', EXTSWA_DEFAULT_ENABLED_SE);
    add_option('extswa_percent_se', EXTSWA_DEFAULT_PERCENT_SE);

    $charset_collate = ''; 
    if ( version_compare(mysql_get_server_info(), '4.1.0', '>=') )
            $charset_collate = 'DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci';

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
           
        dbDelta("CREATE TABLE IF NOT EXISTS `" . EXTSWA_DB_STATS . "` (
        `type` INT( 1 ) NOT NULL ,
        `count` INT( 15 ) NOT NULL ,
        `data` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
        `date` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
        `source` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
        `links` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
        ) ENGINE=MyISAM $charset_collate");

        dbDelta("CREATE TABLE IF NOT EXISTS `" . EXTSWA_DB_LOG . "` (
          `ID` int(100) NOT NULL AUTO_INCREMENT,
          `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `result` tinyint(1) NOT NULL,
           UNIQUE KEY `ID` (`ID`)
        ) ENGINE=MyISAM $charset_collate AUTO_INCREMENT=1;");
}
function extswa_plugin_deactivation()
{
    global $wpdb;

    delete_option('extswa_file');
    delete_option('extswa_settings');
    delete_option('extswa_exmasks');
    delete_option('extswa_checkpages');
    delete_option('extswa_ajaxpass');

    delete_option('extswa_exextensions');
    delete_option('extswa_extensions');
    delete_option('extswa_mimetypes');
    delete_option('extswa_exmimetypes');
    delete_option('extswa_psereferrers');
    delete_option('extswa_blacklist');
    delete_option('extswa_useragents');

    delete_option('extswa_enabled_se');

    $wpdb->query('DROP TABLE ' . EXTSWA_DB_STATS);
    $wpdb->query('DROP TABLE ' . EXTSWA_DB_LOG);
}

function extswa_admin_generate_menu()
{
    add_menu_page('Плагин Внешнего источника Waspace', 'Ext S WaspAce', 'manage_options', 'extsourcewaspace', 'extswa_print_page_stats');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Генерация Источника', 'manage_options', 'extswa_genextsource', 'extswa_print_page_generation');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Редактировать файл', 'manage_options', 'extswa_refilepage', 'extswa_print_page_editor');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Проверка корректности', 'manage_options', 'extswa_extscorrect', 'extswa_print_page_validation');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Все страницы сайта', 'manage_options', 'extswa_aps', 'extswa_print_page_pages');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Фейковые рефереры', 'manage_options', 'extswa_psereferer_settings', 'extswa_print_page_psereferrers');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Дополнительные UserAgents', 'manage_options', 'extswa_useragent_settings', 'extswa_print_page_useragents');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Маски игнорирования', 'manage_options', 'extswa_exmasks', 'extswa_print_page_exmasks');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Черные списки', 'manage_options', 'extswa_blacklist', 'extswa_print_page_blacklist');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Проверка доступности', 'manage_options', 'extswa_checkpages', 'extswa_print_page_checkpages');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Настройка', 'manage_options', 'extswa_settings', 'extswa_print_page_settings');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Фильтрация', 'manage_options', 'extswa_filtering', 'extswa_print_page_filtering');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'Журнал', 'manage_options', 'extswa_log', 'extswa_print_page_log');
        add_submenu_page( 'extsourcewaspace', 'Внешний источник', 'О плагине', 'manage_options', 'extswa_about', 'extswa_print_page_info');
}

function extswa_load_statistic()
{
    if(!is_admin())
    {
        global $wpdb;
        require_once( 'wp-refers.php' );
    }
}

//Print pages functions
function extswa_print_page_pages()
{
    global $wpdb;
    include( 'pages/_allpages.php' );
}

function extswa_print_page_checkpages()
{
    global $wpdb;
    include( 'pages/_checkpages.php' );
}

function extswa_print_page_useragents()
{
    global $wpdb;
    include( 'pages/_useragents.php' );
}

function extswa_print_page_blacklist()
{
    global $wpdb;
    include( 'pages/_blacklist.php' );
}

function extswa_print_page_log()
{
    global $wpdb;
    include( 'pages/_log.php' );
}

function extswa_print_page_exmasks()
{
    global $wpdb;
    include( 'pages/_exmasks.php' );
}

function extswa_print_page_filtering()
{
    global $wpdb;
    include( 'pages/_filtering.php' );
}

function extswa_print_page_psereferrers()
{
    global $wpdb;
    include( 'pages/_psereferrers.php' );
}
function extswa_print_page_validation()
{
    global $wpdb;
    include( 'pages/_validation.php' );
}

function extswa_print_page_settings()
{
    global $wpdb;
    include( 'pages/_settings.php' );
}

function extswa_print_page_stats()
{
    global $wpdb;
    include( 'pages/_stats.php' );
}

function extswa_print_page_editor()
{
    global $wpdb;
    include( 'pages/_editor.php' );
}

function extswa_print_page_info()
{
    global $wpdb;
    include( 'pages/_about.php' );
}

function extswa_print_page_generation()
{
    global $wpdb;
    include( 'pages/_generation.php' );
}
?>