<?php
	//Support CloudFlare
	$_SERVER['REMOTE_ADDR'] = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR'];

	if(empty($wpdb))
	{
		define( 'SHORTINIT', true );
		include_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
	}

	//Dependence
	foreach(array(
					'plugins_url' => '/wp-includes/link-template.php',
					'trailingslashit' => '/wp-includes/formatting.php',
					'add_action' => '/wp-includes/plugin.php',
					'get_post' => '/wp-includes/post.php',
					'add_rewrite_rule' => '/wp-includes/rewrite.php'
				 ) as $function => $file )
	{
		if(!function_exists($function))
			include_once( $_SERVER['DOCUMENT_ROOT'] . $file );
	}

	if ( empty( $GLOBALS['wp_rewrite'] ) )
		$GLOBALS['wp_rewrite'] = new WP_Rewrite();

	defined('WP_PLUGIN_URL') or define('WP_PLUGIN_URL', home_url().'/wp-content/plugins');

	define( 'EXTSWA_VERSION', '2.5.2' );
	define( 'EXTSWA_DATE', date('d.m.Y'));

	define( 'EXTSWA_AVAILABLE_SE', 'google,qip,rambler,yandex,mail,yahoo,nigma,bing,custom');
	define( 'EXTSWA_DEFAULT_PERCENT_SE', '[]');

	define( 'EXTSWA_DEFAULT_FILENAME', 'wp-extsourcewa.txt');
	define( 'EXTSWA_DEFAULT_ENABLED_SE', EXTSWA_AVAILABLE_SE);
	define( 'EXTSWA_ENABLED_SE', get_option('extswa_enabled_se', EXTSWA_DEFAULT_ENABLED_SE));

	define( 'EXTSWA_PLUGIN_URL', plugins_url() . '/'.@end( explode('/', trim( dirname(__FILE__), '/') ) ) .'/');

	define( 'EXTSWA_EXTFILE_NAME', rtrim(get_option('extswa_file', EXTSWA_DEFAULT_FILENAME),'.txt').'.txt');
	define( 'EXTSWA_EXTFILE_URL', EXTSWA_PLUGIN_URL . EXTSWA_EXTFILE_NAME);
	define( 'EXTSWA_EXTFILE_PATH', plugin_dir_path(__FILE__) . EXTSWA_EXTFILE_NAME);

	define( 'EXTSWA_REPORT_SCRIPT_URL', EXTSWA_PLUGIN_URL . 'wp-reports.php');
	define( 'EXTSWA_AJAX_URL', EXTSWA_PLUGIN_URL . '_ajax.php');

	define( 'EXTSWA_SITE_ROOT_URL', 'http://' . $_SERVER['HTTP_HOST']);

	define( 'EXTSWA_DB_STATS', $wpdb->prefix . 'extswa_stats' );
	define( 'EXTSWA_DB_LOG', $wpdb->prefix . 'extswa_log' );

	define( 'EXTSWA_DEFAULT_AJAXPASS', md5(plugin_dir_path(__FILE__)));
	define( 'EXTSWA_AJAXPASS', get_option('extswa_ajaxpass', EXTSWA_DEFAULT_AJAXPASS));

	//GEO DATABASE
	define('SXFILELIB', plugin_dir_path(__FILE__) . 'GeoDB/SxGeo.dat');

	//Classes and libraries
	define('CLASS_GEO', plugin_dir_path(__FILE__) . 'GeoDB/geo.class.php');
	define('CLASS_UserAgent', plugin_dir_path(__FILE__) . 'classes/useragent.class.php');
	define('CLASS_Referrer', plugin_dir_path(__FILE__) . 'classes/referrer.class.php');
	define('CLASS_Blacklist', plugin_dir_path(__FILE__) . 'classes/blacklist.class.php');
	define('CLASS_Extfile', plugin_dir_path(__FILE__) . 'classes/extfile.class.php');
	define('CLASS_Browser', plugin_dir_path(__FILE__) . 'classes/browser.php');
	define('LIBRARY_SHD', plugin_dir_path(__FILE__) . 'classes/simple_html_dom.php');
	define('LIBRARY_SE', plugin_dir_path(__FILE__) . 'classes/se.functions.php');
	define('CLASS_Statistic', plugin_dir_path(__FILE__) . 'classes/statistic.class.php');
	define('LIBRARY_DEPRECATED_FUNCTIONS', plugin_dir_path(__FILE__) . 'classes/deprecated.functions.php');

	//Temp constants
	foreach(array('STATIC_GENERATION' => 's', 'DYNAMIC_GENERATION' => 'd', 'ENABLED' => 'y', 'DISABLED' => 'n', 'SINGLE_PASS' => 'e') as $name => $value)
		define($name, $value);
?>